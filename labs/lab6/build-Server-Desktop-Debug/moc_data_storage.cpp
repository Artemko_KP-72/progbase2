/****************************************************************************
** Meta object code from reading C++ file 'data_storage.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Server/data_storage.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'data_storage.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Data_Storage_t {
    QByteArrayData data[11];
    char stringdata0[142];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Data_Storage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Data_Storage_t qt_meta_stringdata_Data_Storage = {
    {
QT_MOC_LITERAL(0, 0, 12), // "Data_Storage"
QT_MOC_LITERAL(1, 13, 15), // "getRequestIndex"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 11), // "std::string"
QT_MOC_LITERAL(4, 42, 10), // "requestStr"
QT_MOC_LITERAL(5, 53, 24), // "return_Entity_by_request"
QT_MOC_LITERAL(6, 78, 5), // "index"
QT_MOC_LITERAL(7, 84, 15), // "addEntityToData"
QT_MOC_LITERAL(8, 100, 12), // "removeEntity"
QT_MOC_LITERAL(9, 113, 14), // "changeEntetyIn"
QT_MOC_LITERAL(10, 128, 13) // "requestString"

    },
    "Data_Storage\0getRequestIndex\0\0std::string\0"
    "requestStr\0return_Entity_by_request\0"
    "index\0addEntityToData\0removeEntity\0"
    "changeEntetyIn\0requestString"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Data_Storage[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x0a /* Public */,
       5,    1,   42,    2, 0x0a /* Public */,
       7,    1,   45,    2, 0x0a /* Public */,
       8,    1,   48,    2, 0x0a /* Public */,
       9,    2,   51,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Int, 0x80000000 | 3,    4,
    0x80000000 | 3, QMetaType::Int,    6,
    0x80000000 | 3, 0x80000000 | 3,    4,
    0x80000000 | 3, QMetaType::Int,    6,
    0x80000000 | 3, 0x80000000 | 3, QMetaType::Int,   10,    6,

       0        // eod
};

void Data_Storage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Data_Storage *_t = static_cast<Data_Storage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { int _r = _t->getRequestIndex((*reinterpret_cast< std::string(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 1: { std::string _r = _t->return_Entity_by_request((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< std::string*>(_a[0]) = _r; }  break;
        case 2: { std::string _r = _t->addEntityToData((*reinterpret_cast< std::string(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< std::string*>(_a[0]) = _r; }  break;
        case 3: { std::string _r = _t->removeEntity((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< std::string*>(_a[0]) = _r; }  break;
        case 4: { std::string _r = _t->changeEntetyIn((*reinterpret_cast< std::string(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< std::string*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObject Data_Storage::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Data_Storage.data,
      qt_meta_data_Data_Storage,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Data_Storage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Data_Storage::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Data_Storage.stringdata0))
        return static_cast<void*>(const_cast< Data_Storage*>(this));
    return QObject::qt_metacast(_clname);
}

int Data_Storage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
