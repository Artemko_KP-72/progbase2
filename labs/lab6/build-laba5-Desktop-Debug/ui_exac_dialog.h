/********************************************************************************
** Form generated from reading UI file 'exac_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXAC_DIALOG_H
#define UI_EXAC_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_exac_dialog
{
public:
    QLineEdit *domen;
    QPushButton *start;
    QListWidget *newList;
    QListWidget *oldList;

    void setupUi(QDialog *exac_dialog)
    {
        if (exac_dialog->objectName().isEmpty())
            exac_dialog->setObjectName(QStringLiteral("exac_dialog"));
        exac_dialog->resize(597, 534);
        domen = new QLineEdit(exac_dialog);
        domen->setObjectName(QStringLiteral("domen"));
        domen->setGeometry(QRect(60, 50, 113, 27));
        start = new QPushButton(exac_dialog);
        start->setObjectName(QStringLiteral("start"));
        start->setGeometry(QRect(230, 50, 85, 27));
        newList = new QListWidget(exac_dialog);
        newList->setObjectName(QStringLiteral("newList"));
        newList->setGeometry(QRect(290, 120, 256, 192));
        oldList = new QListWidget(exac_dialog);
        oldList->setObjectName(QStringLiteral("oldList"));
        oldList->setGeometry(QRect(20, 120, 256, 192));

        retranslateUi(exac_dialog);

        QMetaObject::connectSlotsByName(exac_dialog);
    } // setupUi

    void retranslateUi(QDialog *exac_dialog)
    {
        exac_dialog->setWindowTitle(QApplication::translate("exac_dialog", "Dialog", 0));
        domen->setText(QApplication::translate("exac_dialog", ".com", 0));
        start->setText(QApplication::translate("exac_dialog", "find", 0));
    } // retranslateUi

};

namespace Ui {
    class exac_dialog: public Ui_exac_dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXAC_DIALOG_H
