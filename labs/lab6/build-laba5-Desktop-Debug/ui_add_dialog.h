/********************************************************************************
** Form generated from reading UI file 'add_dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADD_DIALOG_H
#define UI_ADD_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_add_dialog
{
public:
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QFormLayout *formLayout_2;
    QLineEdit *name_in;
    QLabel *label_4;
    QLineEdit *author_in;
    QLabel *label_3;
    QDialogButtonBox *buttonBox;
    QDoubleSpinBox *daily_visits_in;
    QSpinBox *position_in;

    void setupUi(QDialog *add_dialog)
    {
        if (add_dialog->objectName().isEmpty())
            add_dialog->setObjectName(QStringLiteral("add_dialog"));
        add_dialog->resize(481, 464);
        formLayoutWidget = new QWidget(add_dialog);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(30, 40, 411, 291));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QStringLiteral("label"));
        QFont font;
        font.setFamily(QStringLiteral("Norasi"));
        font.setPointSize(16);
        label->setFont(font);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        name_in = new QLineEdit(formLayoutWidget);
        name_in->setObjectName(QStringLiteral("name_in"));
        name_in->setFont(font);

        formLayout_2->setWidget(0, QFormLayout::SpanningRole, name_in);


        formLayout->setLayout(0, QFormLayout::FieldRole, formLayout_2);

        label_4 = new QLabel(formLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font);

        formLayout->setWidget(3, QFormLayout::LabelRole, label_4);

        author_in = new QLineEdit(formLayoutWidget);
        author_in->setObjectName(QStringLiteral("author_in"));
        author_in->setFont(font);

        formLayout->setWidget(3, QFormLayout::FieldRole, author_in);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font);

        formLayout->setWidget(4, QFormLayout::LabelRole, label_3);

        buttonBox = new QDialogButtonBox(formLayoutWidget);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setFont(font);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        formLayout->setWidget(5, QFormLayout::FieldRole, buttonBox);

        daily_visits_in = new QDoubleSpinBox(formLayoutWidget);
        daily_visits_in->setObjectName(QStringLiteral("daily_visits_in"));
        daily_visits_in->setMaximum(999999);
        daily_visits_in->setSingleStep(1e-05);

        formLayout->setWidget(1, QFormLayout::FieldRole, daily_visits_in);

        position_in = new QSpinBox(formLayoutWidget);
        position_in->setObjectName(QStringLiteral("position_in"));
        position_in->setMinimum(1);
        position_in->setMaximum(999999);

        formLayout->setWidget(4, QFormLayout::FieldRole, position_in);


        retranslateUi(add_dialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), add_dialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), add_dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(add_dialog);
    } // setupUi

    void retranslateUi(QDialog *add_dialog)
    {
        add_dialog->setWindowTitle(QApplication::translate("add_dialog", "Dialog", 0));
        label->setText(QApplication::translate("add_dialog", "Link", 0));
        label_2->setText(QApplication::translate("add_dialog", "Daily visits", 0));
        name_in->setText(QApplication::translate("add_dialog", "some site", 0));
        label_4->setText(QApplication::translate("add_dialog", "Author", 0));
        author_in->setText(QApplication::translate("add_dialog", "Me)", 0));
        label_3->setText(QApplication::translate("add_dialog", "Position", 0));
    } // retranslateUi

};

namespace Ui {
    class add_dialog: public Ui_add_dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADD_DIALOG_H
