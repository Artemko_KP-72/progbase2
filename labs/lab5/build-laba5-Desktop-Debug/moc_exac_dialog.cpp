/****************************************************************************
** Meta object code from reading C++ file 'exac_dialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../laba5/exac_dialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'exac_dialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_exac_dialog_t {
    QByteArrayData data[9];
    char stringdata0[93];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_exac_dialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_exac_dialog_t qt_meta_stringdata_exac_dialog = {
    {
QT_MOC_LITERAL(0, 0, 11), // "exac_dialog"
QT_MOC_LITERAL(1, 12, 9), // "fillSlots"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 24), // "QList<QListWidgetItem*>*"
QT_MOC_LITERAL(4, 48, 4), // "list"
QT_MOC_LITERAL(5, 53, 9), // "toListAdd"
QT_MOC_LITERAL(6, 63, 8), // "Website*"
QT_MOC_LITERAL(7, 72, 3), // "web"
QT_MOC_LITERAL(8, 76, 16) // "on_start_clicked"

    },
    "exac_dialog\0fillSlots\0\0QList<QListWidgetItem*>*\0"
    "list\0toListAdd\0Website*\0web\0"
    "on_start_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_exac_dialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x08 /* Private */,
       5,    1,   32,    2, 0x08 /* Private */,
       8,    0,   35,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void,

       0        // eod
};

void exac_dialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        exac_dialog *_t = static_cast<exac_dialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->fillSlots((*reinterpret_cast< QList<QListWidgetItem*>*(*)>(_a[1]))); break;
        case 1: _t->toListAdd((*reinterpret_cast< Website*(*)>(_a[1]))); break;
        case 2: _t->on_start_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Website* >(); break;
            }
            break;
        }
    }
}

const QMetaObject exac_dialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_exac_dialog.data,
      qt_meta_data_exac_dialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *exac_dialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *exac_dialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_exac_dialog.stringdata0))
        return static_cast<void*>(const_cast< exac_dialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int exac_dialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
