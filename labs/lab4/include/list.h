#pragma once
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <progbase.h>
#include <progbase/console.h>
#include <string.h>


#define BIG_BUFFER_LENGTH 10000
#define BUFFER_LENGTH 200
#define LINKLEN 100
#define INFLEN 30
#define EPSILON 0.1
#define PERCENT 50
#define SLEEP 125

struct inf {
  char protocol[INFLEN];
  char domen[INFLEN];
};
typedef struct website_node website_node;
struct website {
  char link[LINKLEN];
  int position;
  float daily_visits;
  struct inf site_inf;
};

struct website_node {
  struct website *data;
  struct website_node *next;
};
/*
*/

typedef struct __List List;
typedef struct __LNode LNode;

struct __List {
    LNode *head;
  };
  
  struct __LNode {
    struct website web;
    LNode *next;
  };

List *List_new(void);
void List_free(List *self);

bool List_add(List *self, struct website value);
void List_insert(List *self, size_t index, struct website);
int List_removeAt(List *self, size_t index);
size_t List_count(List *self);
LNode * List_at(List *self, size_t index);

/**
 *  @returns old value at index
 */
int List_set(List *self, size_t index, struct website value);