#include <webactions.h>
int webtostr(char *buffer, struct website *point, int bufferlength);
struct website strtoweb(char *buffer, int bufferlength, int * nread);
void text_to_nodes(List * head, char *buffer, int bufferlength, int amount);
List * read_file_to_nodes(const char *fileName);
bool nodes_to_text(char *buffer, int bufferlength, int amount, List*head);
bool nodes_to_file(List *head);