#include <webactions.h>
void cleanBuffer(void) {
    char c = 0;
    while (c != '\n') {
      c = getchar();
    }
  }


static void node_free(LNode *point) {
    free(point);
  }

bool isEqualy(struct website *a, struct website *b) {
    if (strcmp(a->site_inf.protocol, b->site_inf.protocol) == 0 &&
        strcmp(a->link, b->link) == 0 &&
        strcmp(a->site_inf.domen, b->site_inf.domen) == 0 &&
        (fabs(a->daily_visits - b->daily_visits) < EPSILON))
      return true;
    return false;
  }

bool add_new_entity(List * head) {
    /**/
    struct website donor;
    char local_key = '0';
    while (local_key != '\0') {
      printf("Enter the index\n~> ");
      if (!scanf("%i", &donor.position)) {
        cleanBuffer();
        continue;
      } else
        local_key = '\0';
    }
    local_key = '0';
    printf("Enter the protocol\n~> ");
    scanf("%s", donor.site_inf.protocol);
    printf("Enter the link (without protocol and domen, just name)\n~>");
    scanf("%s", donor.link);
    printf("Enter domen\n~> ");
    scanf("%s", donor.site_inf.domen);
  
    local_key = '0';
    while (local_key != '\0') {
      printf("Enter the average visits per day");
      printf("~> ");
      if (!scanf("%f", &donor.daily_visits)) {
        cleanBuffer();
        continue;
      } else
        local_key = '\0';
    }
    puts("");
    /**/
    return List_add(head, donor);
  }


bool remove_node(List * head_of_list, int index) {
  if (index <= 0 || head_of_list == NULL || head_of_list->head == NULL)
    return false;
  if (index == 1) {
    LNode * copy =  head_of_list->head;
    head_of_list->head = head_of_list->head->next;
    node_free(copy);
    return true;
  }
  LNode* head = List_at(head_of_list, index - 1);  
  if (head != NULL ) {
      LNode * copy = head->next;
      head->next = head->next->next;
      node_free(copy);
      return true;
  }
  else
    return false;
 }


bool print_all_website(List * head) {
      if (head == NULL && head->head == NULL)
        return false;
        LNode * curr = head->head;
      Console_clear();
      int zsuv = -1;
      int count_zsuviv = -1;
      while (curr != NULL) {
        count_zsuviv++;
        if (count_zsuviv % 3 == 0) {
          zsuv++;
          count_zsuviv = 0;
        }
        print_website(curr, zsuv * 50, count_zsuviv * 6);
        sleepMillis(40);
        curr = curr->next; // here can be problem
      } 
      return true;
    }

struct website_node *create_SLNode_list(int count) {
    struct website_node *head = calloc(10, sizeof(struct website_node));
    struct website *data = calloc(10, sizeof(struct website));
    struct website_node *primal = head;
    for (int i = 0; i < count - 1; i++) {
      head[i].data = &data[i];
      head[i].next = &head[i + 1];
    }
    head[count - 1].data = &data[count - 1];
    head[count - 1].next = NULL;
    return primal;
  }


void find_domen(List*head) {
      LNode * curr = head->head;
      int zsuv = -1;
      int count_zsuviv = -1;
      while (curr!= NULL) {
        if (strcmp(curr->web.site_inf.domen, ".com") == 0) {
          count_zsuviv++;
          if (count_zsuviv % 3 == 0) {
            zsuv++;
            count_zsuviv = 0;
          }
          print_website(curr, zsuv * 50, 6 * count_zsuviv);
        }
        curr = curr->next;
      }
    }
void totaly_change_value(List* head, int index) {
    LNode * curr = List_at(head, index);
    if (curr == NULL)
      return ;
    struct website donor;
    char local_key = '0';
  
    donor.position = index;
  
    printf("Enter the protocol");
    printf("~> ");
    scanf("%s", curr->web.site_inf.protocol);
    printf("Enter the link (without protocol and domen, just name)");
    printf("~> ");
    scanf("%s", curr->web.link);
  
    printf("Enter domen\n~> ");
    printf("~> ");
    scanf("%s", curr->web.site_inf.domen);
  
    local_key = '0';
    while (local_key != '\0') {
      printf("Enter the average visits per day");
      printf("~> ");
      if (!scanf("%f", &curr->web.daily_visits)) {
        cleanBuffer();
        continue;
      } else
        local_key = '\0';
    }
    puts("");
    Console_getChar();
  }



  //bool change_value(List*head, const char *argument, char *new_value);
bool change_value(LNode *head, const char *argument, char *new_value) {
    int react = 0;
    int end_of_argument = strlen(new_value);
    if (strcmp(argument, "link") == 0) {
      strcpy(head->web.link, new_value);
      head->web.link[end_of_argument + 1] = '\0';
      react++;
    }
    if (strcmp(argument, "position") == 0) {
      head->web.position = atoi(new_value);
      react++;
    }
    if (strcmp(argument, "daily_visits") == 0) {
      head->web.daily_visits = atof(new_value);
      react++;
    }
    if (strcmp(argument, "protocol") == 0) {
      strcpy(head->web.site_inf.protocol, new_value);
      react++;
    }
    if (strcmp(argument, "domen") == 0) {
      strcpy(head->web.site_inf.domen, new_value);
      react++;
    }
    if (react == 0)
      return false;
    return true;
  }
  


bool change_value_function(List *head) {
    int local_key = '0';
    char parameter[BUFFER_LENGTH];
    char new_value[BUFFER_LENGTH];
    int siteindex = 0;
    while (local_key != '\0') {
      Console_clear();
      puts("\nEnter the index of site");
      if (!scanf("%i", &siteindex)) {
        puts("Maybe you entered incorrect index\nPlease try again :)");
        Console_getChar();
        cleanBuffer();
        continue;
      } else {
        local_key = '\0';
      }
    }
  
    local_key = '0';
  
    while (local_key != '\0') {
      Console_clear();
      print_website(List_at(head, siteindex), -1, -1);
      puts("\nEnter the name of the parameter you are want to change");
      scanf("%s", parameter);
      puts("\nEnter the new value");
      scanf("%s", new_value);
      if (!change_value(List_at(head, siteindex), parameter, new_value)) {
        puts("Maybe you entered incorrect parameter\nPlease try again :)");
        cleanBuffer();
        Console_getChar();
        cleanBuffer();
        continue;
      } else {
        local_key = '\0';
      }
    }
    print_website(List_at(head, siteindex), -1, -1);
    return true;
  }


void totaly_change_value_function(List * head) {
      char local_key = '0';
      char local_key_helper = '\0';
      int index = 0;
      LNode * curr = NULL;

      while (local_key != '\0') {
        printf("\nEnter the index\n~> ");
        if (!scanf("%i", &index) || index < 1) {
          Console_clear();
          cleanBuffer();
          printf("\nSorry, something is incorrect, please enter the position "
                 "again\n\n      (position shoud be bigger then 0 )\n");
          continue;
        } else {
          printf("\nAre you sure that you want to rewrite this element? (Y/N)\nIf you want to escape press 'e'\n");
          curr = List_at(head, index);
          if (curr == NULL)
            return ;
          print_website(curr, -1, -1);
          local_key_helper = Console_getChar();
          if (local_key_helper == 'Y' || local_key_helper == 'y')
            local_key = '\0';
          if (local_key_helper == 'E' || local_key_helper == 'e')
            return ;
        }
      }
      
      printf("\nEnter position\n~> ");
      while (!scanf("%i", &index)) {
        printf("\nEnter position\n~> ");
        cleanBuffer();
      }

      Console_setCursorPosition(2, 70);
      printf("Enter the protocol");
      Console_setCursorPosition(3, 70);
      printf("~> ");
      scanf("%s", curr->web.site_inf.protocol);
    
      Console_setCursorPosition(5, 70);
      printf("Enter the link (without protocol and domen, just name)");
      Console_setCursorPosition(6, 70);
      printf("~> ");
      scanf("%s", curr->web.link);
    
      Console_setCursorPosition(8, 70);
      printf("Enter domen\n~> ");
      Console_setCursorPosition(9, 70);
      printf("~> ");
      scanf("%s", curr->web.site_inf.domen);
    
      local_key = '0';
      while (local_key != '\0') {
        Console_setCursorPosition(11, 70);
        printf("Enter the average visits per day");
        Console_setCursorPosition(12, 70);
        printf("~> ");
        if (!scanf("%f", &curr->web.daily_visits)) {
          cleanBuffer();
          continue;
        } else
          local_key = '\0';
      }
      puts("\n\n\n\n\n\n");
      print_website(curr, -1, -1);
      Console_getChar();
    }
    

static void print_website(LNode * curr, int column, int row) {
  if (curr == NULL)
    return ;
    struct website website = curr->web;
      puts("");
    
      if (column != -1)
        Console_setCursorPosition(1 + row, column);
      Console_setCursorAttribute(FG_DEFAULT);
      printf("link is ");
      Console_setCursorAttribute(FG_RED);
      printf("%s%s%s\n", website.site_inf.protocol, website.link,
             website.site_inf.domen);
    
      if (column != -1)
        Console_setCursorPosition(2 + row, column);
      Console_setCursorAttribute(FG_DEFAULT);
      printf("list position ");
      Console_setCursorAttribute(FG_RED);
      printf("%i\n", website.position);
    
      if (column != -1)
        Console_setCursorPosition(3 + row, column);
      Console_setCursorAttribute(FG_DEFAULT);
      printf("average number of visitors per day ");
      Console_setCursorAttribute(FG_RED);
      printf("%.2f\n", website.daily_visits);
    
      if (column != -1)
        Console_setCursorPosition(4 + row, column);
      Console_setCursorAttribute(FG_DEFAULT);
      printf("protocol is ");
      Console_setCursorAttribute(FG_RED);
      printf("%s\n", website.site_inf.protocol);
    
      if (column != -1)
        Console_setCursorPosition(5 + row, column);
      Console_setCursorAttribute(FG_DEFAULT);
      printf("domen is ");
      Console_setCursorAttribute(FG_RED);
      printf("%s\n", website.site_inf.domen);
      Console_setCursorAttribute(FG_DEFAULT);
    }

