#include <transform.h>
int webtostr(char *buffer, struct website *point, int bufferlength) {
  return snprintf(buffer, bufferlength, "%s,%s,%s,%i,%.2lf\n",
                  point->site_inf.protocol, point->link, point->site_inf.domen,
                  point->position, point->daily_visits);
}

struct website strtoweb(char *buffer, int bufferlength, int * nread) {
  struct website current;
  if (buffer == NULL)
    return current;
  int nread_increment = 0;
  char koma = '\0';
  sscanf(buffer, "%[^,]%*c %[^,]%*c %[^,]%*c %i %c %f %n",
         current.site_inf.protocol, current.link, current.site_inf.domen,
         &current.position, &koma, &current.daily_visits, &nread_increment);
         *nread += nread_increment;
  return current;
}

void text_to_nodes(List * head, char *buffer, int bufferlength, int amount) {
  int step = 0;
  for (int i = -1; step < strlen(buffer); i++) {
    List_add(head, strtoweb(buffer + step, bufferlength, &step));
  }
}

List * read_file_to_nodes(const char *fileName) {
  List * head = List_new();
  FILE *f;
  char big_buffer[BIG_BUFFER_LENGTH];
  if ((f = fopen("input.txt", "rb")) == NULL) {
    printf("Cannot open file.\n");
  }
  fread(big_buffer, 1, BIG_BUFFER_LENGTH, f);
  int step = 0;
  for (int i = -1; /*Need to correct*/ step < strlen(big_buffer) /*Need to correct*/; i++) {
    List_add(head, strtoweb(big_buffer + step, BIG_BUFFER_LENGTH, &step));
  }
  free(f);
  return head;
}

bool nodes_to_text(char *buffer, int bufferlength, int amount, List *head) {
  if (buffer == NULL || head == NULL || head->head == NULL)
    return false;
  int step = 0;
  LNode *curr = head->head;
  while (curr != NULL){
      step += webtostr(buffer + step, &curr->web, bufferlength);
      curr = curr->next; 
  }
  return true;
}

bool nodes_to_file(List *head) {
  if (head == NULL)
    return false;
  Console_clear();
  char out[BUFFER_LENGTH] = "output.txt";
  char tofile[BIG_BUFFER_LENGTH];
  printf("Enter the output file name\n*if you want use standart out put just "
         "enter space \n~> ");
  scanf("%s", out);
  if (out[0] ==  ' ')
    strcpy(out, "output.txt\n");
  nodes_to_text(tofile, BIG_BUFFER_LENGTH, 11, head);
  FILE *f = fopen(out, "wb");
  fprintf(f, "%s", tofile);
  fclose(f);
  return true;
}