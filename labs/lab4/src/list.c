#include <list.h>

static LNode *LNode_new(struct website value) {
  LNode *new = malloc(sizeof(LNode));
/* 
*/
  if (new != NULL) {
    strcpy(new->web.link, value.link);
    new->web.position = value.position;
    new->web.daily_visits = value.daily_visits;
    strcpy(new->web.site_inf.domen, value.site_inf.domen);
    strcpy(new->web.site_inf.protocol, value.site_inf.protocol);
    new->next = NULL;
  }
  return new;
}

List *List_new(void) {
  List *new = malloc(sizeof(List));
  return new;
}

 static void recursion_delite(LNode *copy) {
  if (copy->next != NULL)
    recursion_delite(copy->next);
  free(copy);
}

void List_free(List *self) {
  if (self == NULL)
    return;
  if (self->head != NULL)
    recursion_delite(self->head);
  free(self);
}

bool List_add(List *self, struct website value) {
  if (self == NULL)
    return false;
  LNode *copyself = self->head;
  if (copyself != NULL) {
    while (copyself->next != NULL) {
      copyself = copyself->next;
    }
    copyself->next = LNode_new(value);
  } else {
    self->head = LNode_new(value);
  }
  return true;
}

void List_insert(List *self, size_t index, struct website value) {
  if (self == NULL) {
    return;
  }
  LNode *copyself = self->head;
  int counter = 1;
  while (copyself->next != NULL && counter != index) {
    copyself = copyself->next;
    counter++;
  }
  if (copyself->next != NULL) {
    // if 1 2 .. 3 4
    LNode *temp = copyself->next->next;
    copyself->next = LNode_new(value);
    copyself->next->next = temp;
  } else {
    // if 1 2 3 4 ..
    copyself->next = LNode_new(value);
  }
}

size_t List_count(List *self) {
  if (self == NULL) {
    return 0;
  }
  LNode *copyself = self->head;
  int count = 1;
  while (copyself->next != NULL) {
    copyself = copyself->next;
    count++;
  }
  return count;
}

LNode * List_at(List *self, size_t index) {
  if (self == NULL)
    return NULL;
  LNode *copyself = self->head;
  int counter = 1;
  while (copyself != NULL) {
    if (counter == index)
      return copyself;
    counter++;
    copyself = copyself->next;
  }
  return NULL;
}

int List_set(List *self, size_t index, struct website value) {
  if (self == NULL)
    return 1;
  LNode *copyself = self->head;
  int counter = 1;
  while (copyself->next != NULL) {
    copyself = copyself->next;
    counter++;
    if (counter == index) {
      copyself->next->web.position = value.position; // can be completed
      return 0;
    }
  }
  return 1;
}

int List_removeAt(List *self, size_t index) {
  if (self == NULL || List_at(self, index) == NULL)
    return 1;
  if (index == 1) {
    LNode *tmp = self->head;
    self->head = self->head->next;
    free(tmp);
    return 0;
  }
  LNode *copyself = self->head;
  int counter = 2;
  while (copyself->next != NULL) {
    if (counter == index) {
      LNode *temp = copyself->next;
      copyself->next = copyself->next->next;
      free(temp);
      return 0;
    }
    copyself = copyself->next;
    counter++;
  }
  return 1;
}
