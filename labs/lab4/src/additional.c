#include <additional.h>
static void cleanBuffer(void) {
    char c = 0;
    while (c != '\n') {
      c = getchar();
    }
  }

void menu1(void) {
    int switcher = 1;
    char local_key = '\0';
    char key = '\0';
    int names_count = 3;
    char names2[] = "Fill nodes from data store#Create new array#Go back";
    char names[names_count][BUFFER_LENGTH];
    int nread = 0;
    int step = 0;
    int count = 0;
    List * head = NULL;
    for (int i = 0; i < names_count; i++) {
      sscanf(names2 + step, "%[^#]%*c%n", names[i], &nread);
      step += nread;
    }
    Console_hideCursor();
  
    while (key != 'E') {
  
      Console_clear();
  
      puts("Welcome to my program\n");
      for (int i = 0; i < names_count; i++) {
        if (switcher == i + 1)
          Console_setCursorAttribute(FG_YELLOW);
        printf("%s\n", names[i]);
  
        Console_setCursorAttribute(FG_DEFAULT);
      }
      key = Console_getChar();
  
      if (key == 10) {
  
        if (switcher == 1) {
          head = read_file_to_nodes("../input.txt");
          menu2(head);
          //free(head);
        }
  
        if (switcher == 2) {
          local_key = '0';
          while (local_key != '\0') {
            puts("\nplease enter the required number of sites");
            if (!scanf("%i", &count)) {
              Console_clear();
              cleanBuffer();
              continue;
            } else
              local_key = '\0';
          }
          head = List_new();
          menu2(head);
          //free(head);
        }

        if (switcher == 3) {
          key = 'E';
          Console_clear();
        }
      }
  
      if (key == 66)
        switcher++;
  
      if (key == 65)
        switcher--;
  
      if (switcher == 0)
        switcher = names_count;
  
      if (switcher == names_count + 1)
        switcher = 1;
    }
    List_free(head);
  }

void some_additional_function(void) {
    Console_clear();
    conHideCursor();
    int n = 0;
    int i = 0;
    int j = 0;
    struct ConsoleSize console = Console_size();
    int pieceOFSnow[200][400];
    for (i = 0; i < 200; i++) {
      for (j = 0; j < 400; j++) {
        pieceOFSnow[i][j] = -1;
      }
    }
    srand(time(NULL));
    Console_setCursorAttribute(FG_GREEN);
    Console_setCursorAttribute(BG_BLACK);
  
    while (1) {
      console = Console_size();
      for (j = 0; j < console.columns; j++) {
        for (i = console.rows - 1; i > 0; i--) {
          pieceOFSnow[i][j] = pieceOFSnow[i - 1][j];
          Console_setCursorPosition(i + 1, j + 1);
          if (pieceOFSnow[i][j] == 1 || pieceOFSnow[i][j] == 2)
            printf("%i", pieceOFSnow[i][j] - 1);
          else
            printf(" ");
        }
        n = rand() % 100 + 1;
        if (n <= PERCENT)
          pieceOFSnow[0][j] = rand() % 2 + 1;
        else
          pieceOFSnow[0][j] = -1;
      }
      sleepMillis(SLEEP);
    }
  }

void titles(void) {
    struct ConsoleSize console = Console_size();
    Console_hideCursor();
    for (int i = 1; i < console.rows + 18; i++) {
      Console_clear();
      Console_setCursorPosition(i - 18, 50);
      if (i > 18 && i < console.rows + 18)
        printf("                       you will see me again :) ");
      Console_setCursorPosition(i - 16, 50);
      if (i > 16 && i < console.rows + 16)
        printf("        Without them I would not have done laboratory work");
      Console_setCursorPosition(i - 14, 50);
      if (i > 14 && i < console.rows + 14)
        printf("          my computer, my cat, my VScode and Dennis Ritchi");
      Console_setCursorPosition(i - 12, 50);
      if (i > 12 && i < console.rows + 12)
        printf("                 Olya, Nastya, Nadya, Amir, Oleg");
      Console_setCursorPosition(i - 10, 50);
      if (i > 10 && i < console.rows + 10)
        printf("              Artem, Nikita, Misha, Yaroslav, Vitya, Sasha");
      Console_setCursorPosition(i - 8, 50);
      if (i > 8 && i < console.rows + 8)
        printf("                     Dima, Dan, Bogdan, Igor");
      Console_setCursorPosition(i - 6, 50);
      if (i > 6 && i < console.rows + 6)
        printf(
            "  Phadynyak Ruslan Anatoliyevych - the author of idea this project");
      Console_setCursorPosition(i - 4, 50);
      if (i > 4 && i < console.rows + 4)
        printf("               I want to express my gratitude to:");
      Console_setCursorPosition(i - 2, 50);
      if (i > 2 && i < console.rows + 2)
        puts("              I hope you enjoyed using my creature");
      Console_setCursorPosition(i, 50);
      if (i < console.rows)
        puts("                     This is my second lab");
      sleepMillis(500);
    }
  }

void menu2(List *head) {
    /*
      Додати у кінець списку нову сутність із вказаними користувачем даними
      Видалити сутність із вказаної позиції у списку
      Перезаписати всі дані полів сутності у вказаній позиції на нововведені
      користувачем (повне оновлення сутності)
      Перезаписати тільки обране поле даних сутності із вказаної позиції у списку
      (часткове оновлення сутності)
      Знайти всі сайти із доменом .com. UPD: Вивести результат у консоль.
      Зберегти поточний список сутностей на файлову систему, запропонувавши
      користувачу ввести назву дискового файлу, у який збережуться всі дані.
  */
    int switcher = 1;
    char local_key = '\0';
    char key = '\0';
    int names_count = 9;
    char names[names_count][BUFFER_LENGTH];
    int nread = 0;
    int step = 0;
    // int count = 0;
    int index = 0;
    char names2[] =
        "Add a new entity to the end of the list with the specified user data#"
        "Delete the entity from the specified position in the list#"
        "Overwrite all entity field data in the specified position on the "
        "user-initiated (full entity update)#"
        "Overwrite only the selected data field of the entity from the specified "
        "position in the list (partial refresh of the entity)#"
        "Find all sites with .com.#"
        "Save the current list of entities to the file system, prompting the "
        "user to enter the name of the disk file, which will store all data.#"
        "Additional function 1#"
        "Additional function 2#"
        "Go back#";
  
    for (int i = 0; i < names_count; i++) {
      sscanf(names2 + step, "%[^#]%*c%n", names[i], &nread);
      step += nread;
    }
    Console_hideCursor();
  
    while (key != 'E') {
  
      Console_clear();
  
      puts("Welcome to my program\n");
      for (int i = 0; i < names_count; i++) {
        if (switcher == i + 1)
          Console_setCursorAttribute(FG_YELLOW);
        printf("%s\n", names[i]);
        Console_setCursorAttribute(FG_DEFAULT);
      }
      key = Console_getChar();
  
      if (key == 10) {
  
        if (switcher == 1) {
          Console_clear();
          add_new_entity(head);
        }
  
        if (switcher == 2) {
          Console_clear();
          print_all_website(head);
          Console_getChar();
          local_key = '0';
          while (local_key != '\0') {
            Console_clear();
            puts("\nEnter the item index you want to delete");
            if (!scanf("%i", &index) || index < 0) {
              index = 0;
              Console_clear();
              cleanBuffer();
              continue;
            } else
              local_key = '\0';
          }
          if (!remove_node(head, index)) {
            printf("Something is going wrong");
            Console_getChar();
          }
          print_all_website(head);
          Console_getChar();
        }
  
        if (switcher == 3) {
          Console_clear();
          totaly_change_value_function(head);
        }
  
        if (switcher == 4) {
          change_value_function(head);
          Console_getChar();
        }
  
        if (switcher == 5) {
          Console_clear();
          find_domen(head);
          Console_getChar();
        }
  
        if (switcher == 6)
          nodes_to_file(head);
  
        if (switcher == 7)
          titles();
  
        if (switcher == 8)
          some_additional_function();
  
        if (switcher == names_count) {
          key = 'E';
          Console_clear();
        }
      }
  
      if (key == 66)
        switcher++;
  
      if (key == 65)
        switcher--;
  
      if (switcher == 0)
        switcher = names_count;
  
      if (switcher == names_count + 1)
        switcher = 1;
    }
  }


bool test(void) {
    return true;
    /*
    List*head = NULL;
    char big_buffer[BIG_BUFFER_LENGTH];
    head = read_file_to_nodes("input.txt");
    struct website somewebsite = {.link = "website",
                                  .position = 1,
                                  .daily_visits = 18.7,
                                  .site_inf = {"https://", ".com"}};
    char str[] = "https:// website .com 1 18.70 ";
    char buffi[BUFFER_LENGTH];
    struct website somewebsite2;
  
    strtoweb(str, strlen(str), &somewebsite2);
    assert(isEqualy(&somewebsite, &somewebsite2) == true);
    change_value(&somewebsite, "position", "6");
    assert(isEqualy(&somewebsite, &somewebsite2) == true);
    change_value(&somewebsite, "domen", ".ua");
    
    assert(isEqualy(&somewebsite, &somewebsite2) == false);
    change_value(&somewebsite, "domen", ".com");
    assert(isEqualy(&somewebsite, &somewebsite2) == true);
    change_value(&somewebsite, "position", "1");
    assert(isEqualy(&somewebsite, &somewebsite2) == true);
  
    webtostr(buffi, &somewebsite2, BUFFER_LENGTH);
    strtoweb(str, strlen(str), &somewebsite2);
    
    char * fake_buff = NULL;
    assert(nodes_to_text(fake_buff, 30, 4, head) == false);
    struct website_node *fake_head = NULL;
    assert(nodes_to_text(buffi, BUFFER_LENGTH, 4, fake_head) == false);
    assert(nodes_to_text(buffi, BUFFER_LENGTH, 10, head) == true);
  
    read_file_to_nodes("output.txt", head, 10);
    webtostr(buffi, head[3].data, BUFFER_LENGTH);
    assert(strcmp(buffi, "https:// math24 .biz 4 18.70 \n") == 0);
    change_value(head[3].data, "domen", ".ua");
    webtostr(buffi, head[3].data, BUFFER_LENGTH);
    assert(strcmp(buffi, "https:// math24 .biz 4 18.70 \n") != 0);
  
    strcpy(big_buffer, "https:// website .com 1 18.70 ");
    assert(strtoweb(big_buffer, BIG_BUFFER_LENGTH, head->data) == strlen(big_buffer));
    assert(strcmp(str, big_buffer) == 0);
    assert(remove_node(head, 3) == true);
    assert(strcmp(str, buffi) != 0);
  
    assert(remove_node(head, 117) == false);
    assert(remove_node(fake_head, 7) == false);
    assert(remove_node(head, 7) == true);
    assert(remove_node(head, 2) == true);
    assert(remove_node(head, 9) == false);
  
    assert(change_value(head[7].data, "domen", ".ua") == true);
    assert(strcmp(head[7].data->site_inf.domen, ".ua") == 0);
    assert(change_value(head[9].data, "daily_visits", "9.498664546435") == true);
    assert(head[7].data->daily_visits - 9.498665 < EPSILON);
  
    assert(change_value(head[2].data, "position", "3"));
    assert(head[2].data->position == 3);
    assert(change_value(head[5].data, "link", "some new link") == true);
    assert(strcmp(head[5].data->link, "some new link") == 0);
    
    assert(change_value(head[1].data, "protocol", "https") == true);
    assert(strcmp(head[1].data->site_inf.protocol, "https") == 0);
    assert(change_value(head[1].data, "daily_visitos", "9") == false);
    assert(change_value(head[1].data, "daily_visits", "9"));
    assert(change_value(head[1].data, "dumen", "9") == false);
    
    assert(head[1].data->daily_visits == 9.00);
    assert(change_value(head[2].data, "position", "3") == true);
    assert(head[2].data->position == 3);
    assert(change_value(head[2].data, "posushon", "3") == false);
    assert(nodes_to_file(fake_head) == 0);
   
    free(head);
    return true;
    */
  }



