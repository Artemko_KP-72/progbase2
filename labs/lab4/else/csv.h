#pragma once
#include <stringbuffer.h>
#include <list.h>

typedef struct __CsvTable CsvTable;
typedef struct __CsvRow   CsvRow;

#define foreach(VARTYPE, VARNAME, LIST)   \
for (int VARNAME##_i = 0, VARNAME##_length = List_count(LIST); !VARNAME##_i; ++VARNAME##_i) \
    for (VARTYPE VARNAME = (VARTYPE)List_at(LIST, VARNAME##_i); \
        VARNAME##_i < VARNAME##_length; \
        VARNAME = (VARTYPE)List_at(LIST, ++VARNAME##_i))

CsvRow * CsvRow_new(void);
void CsvRow_free(CsvRow * self);
void CsvRow_add(CsvRow * self, const char * value);
void CsvTable_values(CsvRow * self, List * values);

CsvTable * CsvTable_new(void);
void CsvTable_free(CsvTable * self);
void CsvTable_add (CsvTable * self, CsvRow * row);
void CsvTable_rows(CsvTable * self, List * rows);

CsvTable * CsvTable_newFromString(const char * csvString);
char *     CsvTable_toNewString  (CsvTable * self);
