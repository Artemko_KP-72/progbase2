
#include <stringbuffer.h>
#include <stdlib.h>
#include <string.h>


struct __StringBuffer {
    char * buffer;
    size_t capacity;
    size_t length;
};

static const int INITIAL_CAPACITY = 256;

StringBuffer * StringBuffer_new(void) {
    StringBuffer * self = malloc(sizeof(StringBuffer));
    self->capacity = INITIAL_CAPACITY;
    self->buffer = malloc(sizeof(char) * self->capacity);
    self->buffer[0] = '\0';
    self->length = 1;
    return self;
}

void StringBuffer_free(StringBuffer * self) {
    free(self->buffer);
    free(self);
}

    static void ensureCapacity(StringBuffer * self, const char * str) {
        if (strlen(str) + self->length > self->capacity) {
            size_t newCapacity =  self->capacity * 2;
            char * newBuffer = realloc(self->buffer, sizeof(char) * newCapacity);
            if (newBuffer == NULL) {
                // TODO error
            } else {
                self->buffer = newBuffer;
                self->capacity = newCapacity;
            }
        }
    }

void StringBuffer_append(StringBuffer * self, const char * str) {
    size_t strlength = strlen(str);
    ensureCapacity(self, str);
    strcat(self->buffer + self->length - 1, str);
    self->length += strlength;
}
void StringBuffer_appendChar(StringBuffer * self, char ch) {
    self->buffer[self->length] = '\0';
    self->buffer[self->length - 1] = ch;
    self->length++;
}

void StringBuffer_appendFormat(StringBuffer * self, const char * fmt, ...);
void StringBuffer_clear(StringBuffer * self) {
    self->buffer[0] = '\0';
    self->length = 1;
}

char *strdup (const char *s) {
    char *d = malloc (strlen (s) + 1);   // Space for length plus nul
    if (d == NULL) return NULL;          // No memory
    strcpy (d,s);                        // Copy the characters
    return d;                            // Return the new string
}

char * StringBuffer_toNewString(StringBuffer * self) {
    return strdup(self->buffer);
}

