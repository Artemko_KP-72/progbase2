#ifndef NUMLIST
#include <numlist.h>
#include <numlist_ext.h>
#endif

#include <stdio.h>
#include <stdlib.h>

typedef struct __LNode LNode;

struct __NumList {
  LNode *head;
};

struct __LNode {
  int value;
  LNode *next;
};

void NumList_print(NumList *self) {
  if (self == NULL) {
    return;
  }
  LNode *head = self->head;
  while (head != NULL) {
    printf("%i ,", head->value);
    head = head->next;
  }
}

void delete_all_negative(NumList *self) {
  int count = 1;
  LNode *head = self->head;
  while (head != NULL) {
    if (head->value < 0)
      NumList_removeAt(self, count);
    else {
      count++;
    }
    if (head != NULL)
      head = head->next;
  }
}

double NumList_average(NumList *self) {
  if (self == NULL)
    return -0.0;
  LNode *head = self->head;
  int sum = 0;
  int counter = 0;
  ;
  while (head != NULL) {
    sum += head->value;
    counter++;
  }
  return (sum * 1.0) / counter;
}
int NumList_minIndex(NumList *self) {
  if (self == NULL)
    return -101;
  LNode *head = self->head;
  int min = head->value;
  head = head->next;
  while (head != NULL) {
    if (min > head->value)
      min = head->value;
  }
  return min;
}

int NumList_maxIndex(NumList *self) {
  if (self == NULL)
    return -101;
  LNode *head = self->head;
  int max = head->value;
  head = head->next;
  while (head != NULL) {
    if (max < head->value)
      max = head->value;
  }
  return max;
}
