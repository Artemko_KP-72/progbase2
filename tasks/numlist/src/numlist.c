#ifndef NUMLIST
#include <numlist.h>
#include <numlist_ext.h>
#endif

typedef struct __LNode LNode;

struct __NumList {
  LNode *head;
};

struct __LNode {
  int value;
  LNode *next;
};
LNode *LNode_new(int val) {
  LNode *new = malloc(sizeof(LNode));

  if (new != NULL) {
    new->value = val;
    new->next = NULL;
  }
  return new;
}

NumList *NumList_new(void) {
  NumList *new = malloc(sizeof(NumList));
  return new;
}

void recursion_delite(LNode *copy) {
  if (copy->next != NULL)
    recursion_delite(copy->next);
  free(copy);
}

void NumList_free(NumList *self) {
  if (self == NULL)
    return;
  if (self->head != NULL)
    recursion_delite(self->head);
  free(self);
}

void NumList_add(NumList *self, int value) {
  if (self == NULL)
    return;
  LNode *copyself = self->head;
  if (copyself != NULL) {
    while (copyself->next != NULL) {
      copyself = copyself->next;
    }
    copyself->next = LNode_new(value);
  } else {
    self->head = LNode_new(value);
  }
}

void NumList_insert(NumList *self, size_t index, int value) {
  if (self == NULL) {
    return;
  }
  LNode *copyself = self->head;
  int counter = 1;
  while (copyself->next != NULL && counter != index) {
    copyself = copyself->next;
    counter++;
  }
  if (copyself->next != NULL) {
    // if 1 2 .. 3 4
    LNode *temp = copyself->next->next;
    copyself->next = LNode_new(value);
    copyself->next->next = temp;
  } else {
    // if 1 2 3 4 ..
    copyself->next = LNode_new(value);
  }
}

size_t NumList_count(NumList *self) {
  if (self == NULL) {
    return 0;
  }
  LNode *copyself = self->head;
  int count = 1;
  while (copyself->next != NULL) {
    copyself = copyself->next;
    count++;
  }
  return count;
}

int NumList_at(NumList *self, size_t index) {
  if (self == NULL)
    return -101;
  LNode *copyself = self->head;
  int counter = 1;
  while (copyself != NULL) {
    if (counter == index)
      return copyself->value;
    counter++;
    copyself = copyself->next;
  }
  return -101;
}

int NumList_set(NumList *self, size_t index, int value) {
  if (self == NULL)
    return 1;
  LNode *copyself = self->head;
  int counter = 1;
  while (copyself->next != NULL) {
    copyself = copyself->next;
    counter++;
    if (counter == index) {
      copyself->next->value = value;
      return 0;
    }
  }
  return 1;
}

int NumList_removeAt(NumList *self, size_t index) {
  if (self == NULL || NumList_at(self, index) == -101)
    return 1;
  if (index == 1) {
    LNode *tmp = self->head;
    self->head = self->head->next;
    free(tmp);
    return 0;
  }
  LNode *copyself = self->head;
  int counter = 2;
  while (copyself->next != NULL) {
    if (counter == index) {
      LNode *temp = copyself->next;
      copyself->next = copyself->next->next;
      free(temp);
      return 0;
    }
    copyself = copyself->next;
    counter++;
  }
  return 1;
}
