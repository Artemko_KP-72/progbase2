#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>
#include <stdio.h>
#ifndef NUMLIST
#include <numlist.h>
#include <numlist_ext.h>
#endif


int main(const int argc, const char* argv[]) {
	NumList * head = NumList_new();
	srand(time(NULL));
	for (int i = 2; i < 10; i++) {
		NumList_add(head,rand() % 201 - 100);
	}

	NumList_print(head);
	puts("");
	delete_all_negative(head);
	NumList_print(head);
	puts("");
	NumList_free(head);
	return 0;
}
