#include <list.h>

struct __List {
  LNode *head;
};

struct __LNode {
  void *value;
  LNode *next;
};

LNode *LNode_new(void *val) {
  LNode *new = malloc(sizeof(LNode));

  if (new != NULL) {
    new->value = val;
    new->next = NULL;
  }
  return new;
}

List *List_new(void) {
  List *new = malloc(sizeof(List));
  return new;
}

void recursion_delite(LNode *copy) {
  if (copy->next != NULL)
    recursion_delite(copy->next);
  free(copy->value);
  free(copy);
}

void List_free(List *self) {
  if (self == NULL)
    return;
  if (self->head != NULL)
    recursion_delite(self->head);
  free(self);
}

void List_add(List *self, void *value) {
  if (self == NULL)
    return;
  LNode *copyself = self->head;
  if (copyself != NULL) {
    while (copyself->next != NULL) {
      copyself = copyself->next;
    }
    copyself->next = LNode_new(value);
  } else {
    self->head = LNode_new(value);
  }
}

void List_insert(List *self, size_t index, void *value) {
  if (self == NULL) {
    return;
  }
  LNode *copyself = List_at(self, index);
  if (copyself->next != NULL) {
    // if .. 2 3 4
    if (copyself == self->head) {
      LNode *temp = LNode_new(value);
      temp->next = self->head;
      self->head = temp;
    }
    // if 1 2 .. 3 4
    LNode *temp = copyself->next->next;
    copyself->next = LNode_new(value);
    copyself->next->next = temp;
  } else {
    // if 1 2 3 4 ..
    copyself->next = LNode_new(value);
  }
}

size_t List_count(List *self) {
  if (self == NULL) {
    return 0;
  }
  LNode *copyself = self->head;
  int count = 1;
  while (copyself->next != NULL) {
    copyself = copyself->next;
    count++;
  }
  return count;
}

LNode * List_at(List *self, size_t index) {
  if (self == NULL)
    return NULL;
  LNode *copyself = self->head;
  int counter = 1;
  while (copyself != NULL) {
    if (counter == index)
      return copyself;
    counter++;
    copyself = copyself->next;
  }
  return NULL;
}

void List_set(List *self, size_t index, void *value) {
  if (self == NULL)
    return ;
  LNode *copyself = self->head;
  int counter = 1;
  while (copyself->next != NULL) {
    copyself = copyself->next;
    counter++;
    if (counter == index) 
      copyself->next->value = value;
  }
}

bool List_removeAt(List *self, size_t index) {
  if (self == NULL || List_at(self, index) == NULL)
    return false;
  if (index == 1) {
    LNode *tmp = self->head;
    self->head = self->head->next;
    free(tmp);
    return true;
  }
  LNode *copyself = self->head;
  int counter = 2;
  while (copyself->next != NULL) {
    if (counter == index) {
      LNode *temp = copyself->next;
      copyself->next = copyself->next->next;
      free(temp);
      return 0;
    }
    copyself = copyself->next;
    counter++;
  }
  return false;
}
