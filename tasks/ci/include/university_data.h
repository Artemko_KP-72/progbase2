#include <list.h>

// Instead of creating a new list, I use already developed

typedef struct _Student Student;

typedef struct _Lecturer {
    List * Students;
    char Name[45];
} Lecturer;

struct _Student {
    float score;
    char Group[5];
    char Name[45];
};

stud_list_from_csv(char * path);
Формування списку студентів із CSV строки.
Формування CSV строки із списку студентів.
Прикріплення списку студентів до викладача.
Функція, що приймає двох викладачів і в результаті створює список зі студентами, що прив'язані першого викладача, але не значаться у списку другого.
