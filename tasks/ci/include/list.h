#include <stdlib.h>
#include <stdbool.h>

typedef struct __List List;
typedef struct __LNode LNode;

List * List_new(void);
void List_free(List * self);

void List_add(List * self, void * value);
void List_insert(List * self, size_t index, void * value);
bool List_removeAt(List * self, size_t index);
size_t List_count(List * self);
LNode * List_at(List * self, size_t index);

/**
 *  @returns old value at index
 */
void List_set(List * self, size_t index, void * value);