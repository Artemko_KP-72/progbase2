#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <progbase/events.h>
#include <progbase/console.h>
/*
// importent to remember
	StartEventTypeId
	UpdateEventTypeId
	ExitEventTypeId:
*/


enum {
	KeyClick,
	JustClick
};


typedef struct Members Members;
struct Members {
	char * names [5][20] ;
	bool  available [5];
};

Members * chat_new(void) {
	Members * chat = malloc(sizeof(Members));
	*chat->names[0] = "Jack";       chat->available[0] = true;
	*chat->names[1] = "Olya";       chat->available[1] = true;
	*chat->names[2] = "Taras";      chat->available[2] = true;
	*chat->names[3] = "Lee";       	chat->available[3] = true;
	*chat->names[4] = "Yaroslav";   chat->available[4] = true;
	return chat;
}
void printMember(Members * chat);
void checkInput(EventHandler * self, Event * event);
void regular(EventHandler * self, Event * event);
void keyCodeReactor(EventHandler * self, Event * event);

int main(void) {
	Members * chat = chat_new();
	Console_lockInput();
	// startup event system
	EventSystem_init();

	// add event handlers
	EventSystem_addHandler(EventHandler_new(NULL, NULL, regular));

	EventSystem_addHandler(EventHandler_new(chat, free, keyCodeReactor));

	// start infinite event loop
	EventSystem_loop();
	// cleanup event system
	EventSystem_cleanup();
	Console_unlockInput();
	return 0;
}

/* event handlers functions implementations */

void regular(EventHandler * self, Event * event) {
	switch (event->type) {
		case StartEventTypeId: {
			puts("\n"
			"Створити обробник, що на нажаття кнопок [1-5] генерує події входу/виходу людей із текстового чату."
			"Створити обробника, що позначає чат із учасниками, обробляє події від першого обробника і виводить список учасників чату.."
			"\n");
			break;
		}
		case UpdateEventTypeId: {
			checkInput(self, event);
			break;
		}
		case ExitEventTypeId: {
			puts("");
			puts("<<<EXIT!>>>");
			puts("");
			break;
		}
	}
}

void keyCodeReactor(EventHandler * self, Event * event) {
	switch (event->type) {
		case KeyClick: {
			int * b = (int *)event->data;
			printf("\nCode '%c' \n", (char)*b);
				for (int i = 0; i < 5; i++) {
					if (*b == i + '1') {
						bool * temp= (bool * )((Members *)(self->data))->available + i; // this is working but i can`t explain how
						if (*temp == false)
							* temp = true;
						else
							* temp = false;
					}
				}
			printMember(self->data);
			break;
		}
		case JustClick: {
			int * b = (int *)event->data;
			printf("\nKey pressed '%c' [%i]\n", (char)*b, (char)*b);
			break;
		}
	}
}

void checkInput(EventHandler * self, Event * event) {
	if (conIsKeyDown()) {  // non-blocking key input check
		char * keyCode = malloc(sizeof(char));
		*keyCode = getchar();
		if (*keyCode == 'q' || *keyCode == 27) {
			free(keyCode);
			EventSystem_exit();
		}
		else {
			if (*keyCode >= '1' && *keyCode <= '5') {
				EventSystem_emit(Event_new(self, KeyClick, keyCode, free));
			}
			else 
				EventSystem_emit(Event_new(self, JustClick, keyCode, free));
		}
	}
}

void printMember(Members * chat) {
	for (int i = 0; i < 5; i++) {
		printf("%s is ", *chat->names[i]);
		if (chat->available[i] == true)
			printf("online");
		else
			printf("bloked");
		puts("\n===========================");
	}
}