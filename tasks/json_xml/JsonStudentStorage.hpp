#pragma once
#include "student_storage.hpp"
#include <QDebug>
#include <QtXml>
#include <vector>
#include <iostream>
#include <fstream>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <jansson.h>
#include <cstdlib>


class JsonEntityStorage:StudentStorage {
public:
    JsonEntityStorage(std::string & name):StudentStorage(name){}

    std::vector<Student> load();
    void save(std::vector<Student> & entities);
};

