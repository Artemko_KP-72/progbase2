#include "XmlStudentStorage.hpp"
#include <string>
#include <massage_exeption.hpp>
#include <fstream>
#include "student.hpp"
std::string groupToXmlString(std::vector<Student> & g);
std::vector<Student> xmlStringToGroup(std::string & str);

std::vector<Student> XmlEntityStorage::load() {
    std::ifstream load;
    load.open(this->name());
    if (!load.is_open()) {
        std::string errorMes = "Incorect file name";
        throw(MessageException(errorMes));
        return std::vector<Student>();
    }

    std::string file;
    getline(load, file, '\0');
    load.close();
    //
    return xmlStringToGroup(file);
}

void XmlEntityStorage::save(std::vector<Student> & entities) {
    if (entities.empty()) {
        std::string errorMes = "empty vector";
        throw(MessageException(errorMes));
        return;
    }
    std::ofstream save;
    save.open(this->name());
    save << groupToXmlString(entities);
    save.close();
}

std::vector<Student> xmlStringToGroup(std::string & str) {
    std::vector<Student> result;
    QDomDocument doc;
    if (!doc.setContent(QString::fromStdString(str))) {
        std::string errorMes = "failed parsing xml";
        throw(MessageException(errorMes));
        return std::vector <Student>();
    }

    QDomElement root = doc.documentElement();

    for (int i = 0; i < root.childNodes().length(); i++) {
        QDomNode studentNode = root.childNodes().at(i);
        QDomElement studentEl = studentNode.toElement();
        Student s;
        s.set_name(studentEl.attribute("name").toStdString());
        s.set_age(studentEl.attribute("age").toInt());
        s.set_score(atof((studentEl.attribute("score").toStdString()).c_str()));
        // s.vector_from_str(studentEl.attribute("subjects").toStdString());

        for (int j = 0; j < studentEl.childNodes().length(); j++) {
            QDomNode subjectNode = studentEl.childNodes().at(j);
            QDomElement subjectEl = subjectNode.toElement();
            QString temp = subjectEl.attribute("subject");
            s.add_subject(temp.toStdString().c_str());
        }
        result.push_back(s);
    }
    return result;
}

std::string groupToXmlString(std::vector<Student> & g) {
    QDomDocument doc;
    QDomElement groupEl = doc.createElement("group");
//    QDomText txtNode = doc.createTextNode("Chapter");
//    groupEl.setAttribute("name", "List of students");
//   groupEl.appendChild(txtNode);

    for (Student & s : g) {
        QDomElement studentEl = doc.createElement("student");
        studentEl.setAttribute("name", s.get_name().c_str());
        studentEl.setAttribute("age", s.get_age());
        studentEl.setAttribute("score", s.get_score());
        for (std::string & curr: s.get_subjects()) {
            QDomElement temp = doc.createElement("Subject_list");
            temp.setAttribute("subject", curr.c_str());
            studentEl.appendChild(temp);
        }
        // studentEl.setAttribute("subjects", s.get_subjects_str().c_str());

        groupEl.appendChild(studentEl);
    }

    doc.appendChild(groupEl);
    return doc.toString().toStdString();
}
