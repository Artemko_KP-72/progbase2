#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "student.hpp"

class StudentStorage {
     std::string _name;
protected:
     StudentStorage(std::string & name) { this->_name = name; }
public:
     std::string & name() { return this->_name; }

     virtual std::vector<Student> load() = 0;
     virtual void save(std::vector<Student> & entities) = 0;
};
