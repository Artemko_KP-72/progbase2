#pragma once
#include <string>

class MessageException : public std::exception {
    std::string _error;
public:
    MessageException(const std::string & error) {
        this->_error = error;
    }
    const char * what() const noexcept{
        return this->_error.c_str();
    }
};
