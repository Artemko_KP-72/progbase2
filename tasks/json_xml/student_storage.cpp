#include "student_storage.hpp"
#include <iostream>


static std::string read_till_coma(char * line) {
    std::string slot;
    while (line[0] != ',' || line[0] != '\0') {
        slot.push_back(line[0]);
        line++;
    }
    line++;
    return slot;
}

std::vector<Student> StudentStorage::load() {
    std::vector<Student> my_vector;
    std::ifstream load("save1");
    if (!load.is_open()) {
        std::cout << "Empty file" << std::endl;
        return my_vector;
    }
    char buff[50]; // буфер промежуточного хранения считываемого из файла текста
    load.getline(buff, 50); // считали строку из файла

    while (!load.eof()) {
        Student curr = Student(); // need help !!!!!
        curr.set_name(read_till_coma(buff));
        curr.set_age(std::atoi(read_till_coma(buff).c_str()));
        curr.set_score(std::atof(read_till_coma(buff).c_str()));
        // curr.set_subjects(read_till_coma(buff));
        my_vector.push_back(curr);
    }
    load.close(); // закрываем файл
    return my_vector;
}

void StudentStorage::save(std::vector<Student> & entities) {
    std::ofstream save;
    save.open(this->name());
    for(auto& self : entities) {
        save << self.get_name()    << ","
            << self.get_age()      << ","
            << self.get_score()    << ","
            // << self.get_subjects() << ","
            << "\n";
    }
    save.close();
}
