QT += core
QT -= gui
QT += xml
QT += widgets

TARGET = json_xml
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    student.cpp \
    XmlStudentStorage.cpp \
    JsonStudentStorage.cpp \
    StorageManager.cpp

HEADERS += \
    XmlStudentStorage.hpp \
    student_storage.hpp \
    student.hpp \
    StorageManager.hpp \
    massage_exeption.hpp \
    JsonStudentStorage.hpp

DISTFILES += \
    data.json \
    data.xml \
    data.fake

