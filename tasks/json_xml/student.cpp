#include "student.hpp"

Student::Student() {
    Student("Yaroslav", 19, 94.8);
}

Student::Student(std::string _name, int age, double score) {
    this->set_name(_name);
    this->set_age(age);
    this->set_score(score);
}

void Student::add_subject(std::string sub) {
    this->subjects.push_back((sub));
}

void Student::printStudent() {
    std::cout << _name << " | " << age << " | " <<  score << " | " << std::endl << get_subjects_str() << std::endl;
}

void Student::rem_subject(std::string sub) {
    for (unsigned int index = 0; index < this->subjects.size(); index++) {
      if(this->subjects[index] == sub) {
        this->subjects.erase(this->subjects.begin() + index);
          index--;
      }
    }
}

std::string Student::get_name() { return this->_name;}

int Student::get_age() { return this->age;}

double Student::get_score() { return this->score; }

std::vector<std::string> Student::get_subjects() { return this->subjects;}

std::string Student::get_subjects_str() {
    std::string str = "";
    for (unsigned int index = 0; index < this->subjects.size(); index++)
        str = str + " " + this->subjects[index];
    return str;
}

void Student::set_name(std::string name) {this->_name = name;}

void Student::set_age(int age) {this->age = age;}

void Student::set_score(double score) {this->score = score;}


void Student::vector_from_str(std::string str) {
    std::vector<std::string> from_file;
    for (unsigned int i = 0; i < str.length(); i++) {
        std::string gora = "";
        while(str.at(i) != '\n') {
            gora = gora + str.at(i);
            i++;
        }
        this->add_subject(gora);
    }
}

