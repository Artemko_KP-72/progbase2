#include "JsonStudentStorage.hpp"
#include <massage_exeption.hpp>
#include <fstream>
#include "student.hpp"

std::string groupToJsonString(std::vector<Student>  & g, std::string & copy_name);
std::vector<Student> jsonStringToGroup(std::string & str);
std::vector<std::string> jsonHelper(std::string str);

std::vector<Student> JsonEntityStorage::load() {
    std::ifstream load;
    load.open(this->name());
    if (!load.is_open()) {
        std::string errorMes = "Incorect file name";
        throw(MessageException(errorMes));
        return std::vector<Student>();
    }

    std::string file;
    getline(load, file, '\0');
    load.close();
    return jsonStringToGroup(file);
}

void JsonEntityStorage::save(std::vector<Student> & entities) {
    if (entities.empty()) {
        std::string errorMes = "empty vector";
        throw(MessageException(errorMes));
        return;
    }
    std::ofstream save;
    save.open(this->name());
    save << groupToJsonString(entities, this->name());
    save.close();
}

std::vector<Student> jsonStringToGroup(std::string & str) {
    std::vector<Student> result;
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str), &err);
    if (err.error != QJsonParseError::NoError) {
        std::string errorMes = "Failed Parsing .json";
        throw(MessageException(errorMes));
        return std::vector <Student>();
    }

    QJsonObject groupObj = doc.object();
    // this->name = groupObj.value("name").toString(); // have question
    QJsonArray studentsArr = groupObj.value("students").toArray();
    for (int i = 0; i < studentsArr.size(); i++) {
          QJsonValue value = studentsArr.at(i);
          QJsonObject studentObj = value.toObject();

          QJsonArray subjectsArray = studentObj.value("Subjects").toArray();

          result.push_back(Student (
            studentObj.value("name").toString().toStdString(),
            studentObj.value("age").toInt(),
            ((studentObj.value("score")).toString().toDouble())));

          for (int j = 0; j < subjectsArray.size(); j++) {
              QJsonValue value = subjectsArray.at(j);
              result[i].add_subject(value.toString().toStdString());
          }

          // jsonHelper((studentObj.value("subjects").toString().toStdString()));
    }
    return result;
}

std::vector<std::string> jsonHelper(std::string str) {
    std::vector<std::string> from_file;
    for (unsigned int i = 0; i < str.length(); i++) {
        std::string gora = "";
        while(str.at(i) != '\n') {
            gora = gora + str.at(i);
            i++;
        }
        from_file.push_back(gora);
    }
    return from_file;
}


std::string groupToJsonString(std::vector<Student> & g, std::string & copy_name) {
    QJsonDocument doc;
    QJsonObject groupObj;
    groupObj.insert("Chapter", copy_name.c_str());

    QJsonArray studentsArr;
    for (auto & s: g) {
          QJsonObject so;
          so.insert("name", s.get_name().c_str());
          so.insert("age", s.get_age());
          // so.insert("score", s.get_score());
          so.insert("score", QString::number(s.get_score()));
          // so.insert();
          // so.insert("subjects", s.get_subjects_str().c_str());
          QJsonArray subject;
          for (auto & curr: s.get_subjects()) {
              subject.append(curr.c_str());
          }
          so.insert("Subjects", subject);
          studentsArr.append(so);
    }
    groupObj.insert("students", studentsArr);

    doc.setObject(groupObj);
    return doc.toJson().toStdString();
}
