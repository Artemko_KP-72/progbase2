#pragma once
#include "student.hpp"
#include "student_storage.hpp"

class StorageManager {
public:
    static StudentStorage * createStorage(std::string & storageFileName);
};
