#include "StorageManager.hpp"
#include "massage_exeption.hpp"
#include "XmlStudentStorage.hpp"
#include "JsonStudentStorage.hpp"
#include <QString>
#include <string>

StudentStorage * StorageManager::createStorage(std::string & storageFileName) {
  if(QString::fromStdString(storageFileName).endsWith(".json")) {
    return (StudentStorage *)(new JsonEntityStorage(storageFileName));
  }
  else if(QString::fromStdString(storageFileName).endsWith(".xml")) {
    return new XmlEntityStorage(storageFileName);
  }
  else {throw MessageException("Incorrect fileName");}
}


//static StudentStorage * createStorage(std::string & storageFileName) {
//    int i = storageFileName.length() - 5;
//    StudentStorage * to_return;
//    if (storageFileName[i] == '.' && storageFileName[i + 1] == 'j'
//            && storageFileName[i + 2] == 's' && storageFileName[i + 3] == 'o'
//            && storageFileName[i + 4] == 'n') {
//        return NULL;
//        // return (EntityStorage *)(new JsonEntityStorage());
//    }

//    if (storageFileName[i] == '.' && storageFileName[i + 1] == 'x'
//            && storageFileName[i + 2] == 'm' && storageFileName[i + 3] == 'l') {

//        return NULL;
////        XmlEntityStorage * copy_xml = new XmlEntityStorage();
////        to_return = copy_xml;
////        return to_return;
//    } else throw MessageException("Incorrect fileName");

//}


