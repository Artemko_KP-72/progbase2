#pragma once
#include "student_storage.hpp"
#include <exception>
#include <QDebug>
#include <QtXml>
#include <vector>
#include <iostream>

class XmlEntityStorage :public StudentStorage {
public:
    XmlEntityStorage(std::string & name):StudentStorage(name){}

    std::vector<Student> load();
    void save(std::vector<Student> & entities);
};
