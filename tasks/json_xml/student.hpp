#pragma once
#include <string>
#include <vector>
#include <iostream>

class Student { // Entity
    std::string _name;
    int age;
    double score;
    std::vector<std::string> subjects;
public:
    Student();
    Student(std::string _name, int age, double score);
    std::string get_name();
    int get_age();
    double get_score();
    std::vector<std::string> get_subjects();
    std::string get_subjects_str();

    void printStudent();
    void vector_from_str(std::string str);

    void add_subject(std::string sub);
    void rem_subject(std::string sub);

    void set_name(std::string name);
    void set_age(int age);
    void set_score(double score);
};
