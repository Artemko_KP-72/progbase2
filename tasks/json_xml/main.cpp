#include <QCoreApplication>
#include "student.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <QDebug>
#include <massage_exeption.hpp>
#include "student_storage.hpp"
#include "StorageManager.hpp"
#include "JsonStudentStorage.hpp"
#include "XmlStudentStorage.hpp"

using namespace std;

int main() {
    vector<Student> Entity;
    //    Створити і ініціалізувати вектор сутностей (на 3-5 елементів) із довільним вмістом даних.
    Entity.push_back({"Yaroslav", 19, 98.8});
        Entity[0].add_subject("Proga");
        Entity[0].add_subject("Matan");
        Entity[0].add_subject("AeSDe");
    Entity.push_back({"Alina", 18, 74.3});
        Entity[1].add_subject("Dancing");
        Entity[1].add_subject("Vyshyvannya");
        Entity[1].add_subject("Cooking");
    Entity.push_back({"Gena", 23, 87.9});
        Entity[2].add_subject("Strelba");
        Entity[2].add_subject("FP");
        Entity[2].add_subject("Military Training");
    vector<StudentStorage *> self;

    Entity[2].printStudent();
    vector<string> yota ;
    yota.push_back("data.xml");
    yota.push_back("data.json");
    yota.push_back("data.fake");
    for (string & curr : yota) {
        try {
            self.push_back(StorageManager::createStorage(curr));
        } catch(MessageException & error) {
            std::cerr << error.what() << std::endl;
        }
    }

    for (StudentStorage * & curr : self) {
        cout << curr->name() << endl;
        try {
            curr->save(Entity);
        } catch (MessageException & error) {
            cerr << error.what() << endl;
        }
        cout << "saved" << endl;
    }

    cout << "Press any key to continue" << endl;
    getchar();
    for (StudentStorage* & curr: self) {
        cout << "Loading began" << endl;
        try {
            auto temp = curr->load();
            for(auto & t : temp)
                t.printStudent();
        } catch(MessageException & error) {
            cerr << error.what() << endl;
        }
        cout << "Loading finished" << endl;
    }
    return 0;
}
