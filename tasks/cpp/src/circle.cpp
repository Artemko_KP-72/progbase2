#include <circle>

string circle::getName() {
    return this->name;
}

float  circle::getRadius() {
    return this->radius;
}

Color  circle::getColor() {
    return this->color;
}


void circle::setName(string x) {
    this->name = x;
}

void circle::setRadius(float x) {
    this->radius = x;
}

void circle::setColor(Color x ) {
    this->color = x;
}


float  circle::getPerimeter() {
    return this->radius * PI * 2;
}

float  circle::getArea() {
    return this->radius * PI * PI;
}

circle * createObject(string name, float radius, Color color) {
    circle * head = new circle();
    head->setName(name);
    head->setRadius(radius);
    head->setColor(color);
    return head;
}