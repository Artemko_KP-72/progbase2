//#pragma once
#include <menu>
//#include <circle>
static int title_count(string menu_data);
static void title_fill(string menu_data, string *title);
static void menu_print(vector<circle> *l);
static void print_circle(circle curr);
static void add_new_element(vector<circle> *l);
static void print_few_circles(vector<circle> *l, int regime);

using namespace std;

void main_menu(vector<circle> *l) { menu_print(l); }

static void menu_print(vector<circle> *l) {
  while (true) {
    cout << "1 - print all circles data\n"
            "2 - add new circle\n"
            "3 - print part of circles\n"
            "4 - Go back\n\n";
    int choose = 0;
    cin >> choose;
    if (choose == 1)
      print_few_circles(l, -1);

    if (choose == 2)
      add_new_element(l);

    if (choose == 3)
      print_few_circles(l, +1);

    if (choose == 4)
      break;
      cout << "\n\n";
  }
}
static void add_new_element(vector<circle> *l) {
  string name;
  float radius;
  int color;
  cout << "enter the name\n";
  cin >> name;
  cout << "enter the radius\n";
  cin >> radius;
  cout << "enter the color\n 1 - WHITE,\n 2 - RED,\n 3 - GREEN,\n 4 - BLUE,\n 5 - PINK,\n 6 - BLACK\n";
  cin >> color;
  l->push_back(*createObject(name, radius, (Color)color));
  cout << "Circle added to the data\n";
}

static void print_few_circles(vector<circle> *l, int regime) {
  if (l->size() == 0) {
    cout << "You have not any circle, you should add a few";
    return;
  }
  int length = -1;
  if (regime > 0) {
    cout << "Output all circles whose length is greater than the ... ";
    cin >> length;
    cout << "\n\n";
  }
  for (int i = 1; i <= l->size(); i++) {
    if (l->at(i - 1).getRadius() > length) {
      cout << i << " circle\n" << endl;
      print_circle(l->at(i - 1));
    }
  }
}

static void print_circle(circle curr) {
  cout << "name is " << curr.getName() << "| radius is " << curr.getRadius() << "| color is ";
  switch (curr.getColor()) {
    case 1:
      cout << "WHITE\n" << endl;
    break;
    case 2:
      cout << "RED\n" << endl;
    break;
    case 3:
      cout << "GREEN\n" << endl;
    break;
    case 4:
      cout << "BLUE\n" << endl;
    break;
    case 5:
      cout << "PINK\n" << endl;
    break;
    case 6:
      cout << "BLACK\n" << endl;
    break;
    default:
    cout << "UNKNOWN\n" << endl;
    break;
  }
}