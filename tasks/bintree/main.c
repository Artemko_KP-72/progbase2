#include <assert.h>
#include <bintree.h>
#include <bstree.h>
#include <ctype.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define XLEN 10
#define area 15

void makeUnic(int *x, int len);
int fillArray(int * x);

int main(const int argc, const char *argv[]) {
  srand(time(NULL));
  int x[XLEN];
  int counter = fillArray(x);

  BSTree *root = BSTree_new();
  // creating tree
  for (int i = 0; i < counter; i++) {
    BSTree_insert(root, x[i]);
  }

  BSTree_printFormat(root);
  puts("");
  BSTree_printTraverse(root);
  BSTree_clear(root);
  BSTree_free(root);

  return EXIT_SUCCESS;
}

int fillArray(int * x) {
	int counter = 0;
	char key = '\0';
	puts("if you want to fill random press 1, if you want fill by yourself pres "
		 "any other)");
	key = getchar();
	if (key == '1') {
	  counter = XLEN;
	  puts("auto created array\n");
	  for (int i = 0; i < XLEN; i++) {
		x[i] = rand() % area;
		printf("%i ", x[i]);
	  }
	} else {
	  key = '\0';
	  puts("please create your array\n");
	  for (int i = 0; i < XLEN; i++) {
		if (!scanf("%d", &x[i])) {
		  while (1) {
			key = getchar();
			if (key == ',')
			  break;
			if (key == 10) {
			  i = XLEN + 1;
			  break;
			}
		  }
		  i--;
		} else {
		  counter++;
		  if (getchar() != ',')
			i = XLEN;
		}
	  }
	}
	puts("");
	puts("");
	makeUnic(x, counter);
	return counter;
}

void makeUnic(int *x, int len) {
  for (int i = 0; i < len; i++) {
    for (int j = 0; j < len; j++) {
      if (i != j) {
        if (x[j] == x[i]) {
          x[j] = rand() % area;
          j = 0;
          i = 0;
        }
      }
    }
  }
}
