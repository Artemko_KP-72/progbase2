#include <stdlib.h>
#include <stdio.h>
#include <bintree.h>

BinTree * BinTree_new(int value) {
    BinTree * new = malloc(sizeof(BinTree));
    new->left = new->right = NULL;
    new->value = value;
    return new;
}

void BinTree_free(BinTree * self) {
    free(self);
}