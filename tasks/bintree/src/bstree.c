#include <stdlib.h>
#include <stdio.h>
#include <bintree.h>
#include <bstree.h>

typedef struct __BSTree BSTree;

struct __BSTree {
  BinTree *root;
};
//
static void printBinTreeTraverce(BinTree *node);
static void clearBinTree(BinTree *node);
static void insertAtTree(BinTree *node, int key);
static void printBinTree(BinTree *self, int points);
//
BSTree *BSTree_new(void) { return malloc(sizeof(BSTree)); }

void BSTree_free(BSTree *self) {
  if (self != NULL)
    free(self);
}

void BSTree_insert(BSTree *self, int key) {
  if (self == NULL)
    return ;
  if (self->root == NULL) {
    self->root = BinTree_new(key);
  }
  else
    insertAtTree(self->root, key);
}

void BSTree_clear(BSTree *self) {
  if (self != NULL)
    clearBinTree(self->root);
}

// additional
static void clearBinTree(BinTree *node) {
  if (node == NULL)
    return ;
  clearBinTree(node->left);
  clearBinTree(node->right);
  free(node);
}

// aditional

static void insertAtTree(BinTree *node, int key) {
  if (key < node->value) {
    if (node->left != NULL)
      insertAtTree(node->left, key);
    else {
      //printf("left, %i\n", key);
      node->left = BinTree_new(key);
    }
  } else {
    if (node->right != NULL)
      insertAtTree(node->right, key);
    else {
      //printf("right, %i\n", key);
      node->right = BinTree_new(key);
    }
  }
}

void BSTree_printFormat(BSTree *self) {
  if (self != NULL)
    printBinTree(self->root, 0);
}

static void printBinTree(BinTree *node, int point) {
  for (int i = 0; i < 2*point; i++) {
    putchar('.');
  }
  if (node == NULL) {
    printf(" (null)\n"); 
  }
  else {
    printf(" %i\n", node->value);
    printBinTree(node->left, point + 1);
    printBinTree(node->right, point + 1);
  }
 }

void BSTree_printTraverse(BSTree *self) {
  if (self != NULL) {
    printBinTreeTraverce(self->root);
  }
}

static void printBinTreeTraverce(BinTree *node) {
  if (node->left != NULL)
      printBinTreeTraverce(node->left);
  printf("%i ,", node->value);
    if (node->right != NULL)
      printBinTreeTraverce(node->right);
}
