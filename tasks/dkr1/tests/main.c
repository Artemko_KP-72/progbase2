#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <check.h>
#include <stdio.h>

// i write it that way becouse i want launch tests saperetely from dkr1

#include "../include/list.h"
#include "../include/lexer.h"



START_TEST(split_empty_empty) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "");
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 0);
    List_free(tokens);
}
END_TEST

START_TEST(split_oneNumber_oneNumberToken) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "8");
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token *firstToken = (Token *)List_at(tokens, 0);
    Token *testToken = Token_new(TokenType_NUMBER, "8");
    ck_assert(Token_equals(firstToken, testToken));
    Token_free(testToken);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST(split_oneAnd_oneAndToken) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "&&");
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token *firstToken = (Token *)List_at(tokens, 0);
    Token *testToken = Token_new(TokenType_AND, "&&");
    ck_assert(Token_equals(firstToken, testToken));
    Token_free(testToken);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST(split_oneString_oneStringToken) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "\"dratuti\"");
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token *firstToken = (Token *)List_at(tokens, 0);
    Token *testToken = Token_new(TokenType_STRING, "\"dratuti\"");
    ck_assert(Token_equals(firstToken, testToken));
    Token_free(testToken);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST


START_TEST(split_threeToken) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "if()\n");
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 3);
    Token *firstToken = (Token *)List_at(tokens, 0);
    Token *secondToken = (Token *)List_at(tokens, 1);
    Token *thirdToken = (Token *)List_at(tokens, 2);
    Token *testToken = Token_new(TokenType_IF, "if");
    ck_assert(Token_equals(firstToken, testToken));
    testToken = Token_new(TokenType_LPAR, "(");
    ck_assert(Token_equals(secondToken, testToken));
    testToken = Token_new(TokenType_RPAR, ")");
    ck_assert(Token_equals(thirdToken, testToken));
    Token_free(testToken);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST(split_trueToken) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "true ");
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token *firstToken = (Token *)List_at(tokens, 0);
    Token *testToken = Token_new( TokenType_BOOL_EXPR_TRUE, "true");
    ck_assert(Token_equals(firstToken, testToken));
    Token_free(testToken);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST(split_out) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "out");
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token *firstToken = (Token *)List_at(tokens, 0);
    Token *testToken = Token_new( TokenType_OUT, "out");
    ck_assert(Token_equals(firstToken, testToken));
    Token_free(testToken);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST(split_spaces) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "   ");
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 0);
    Token *firstToken = (Token *)List_at(tokens, 0);
    ck_assert(Token_equals(firstToken, NULL));
    Token_free(firstToken);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST(split_number) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "23.7");
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token *firstToken = (Token *)List_at(tokens, 0);
    Token *testToken = Token_new( TokenType_NUMBER, "23.7");
    ck_assert(Token_equals(firstToken, testToken));
    Token_free(testToken);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST(split_variable) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "firt");
    ck_assert_int_eq(status, 1);
    ck_assert_int_eq(List_count(tokens), 0);
    Token *firstToken = (Token *)List_at(tokens, 0);
    Token *testToken = Token_new( TokenType_VAR, "FIRT");
    ck_assert(!Token_equals(firstToken, testToken));
    Token_free(testToken);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST(split_variable2) {
    List *tokens = List_new();
    int status = Lexer_splitTokens(tokens, "uno");
    ck_assert_int_eq(status, 1);
    ck_assert_int_eq(List_count(tokens), 0);
    Token *firstToken = (Token *)List_at(tokens, 0);
    Token *testToken = Token_new( TokenType_VAR, "uno");
    ck_assert(!Token_equals(firstToken, testToken));
    Token_free(testToken);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

Suite *test_suite(void);

int main(const int argc, const char* argv[]) {
	
		Suite *s = test_suite();
		SRunner *sr = srunner_create(s);
		srunner_set_fork_status(sr, CK_NOFORK); // important for debugging!
	
		srunner_run_all(sr, CK_VERBOSE);
	
		int num_tests_failed = srunner_ntests_failed(sr);
		srunner_free(sr);
		return num_tests_failed;

	return EXIT_SUCCESS;
}


Suite *test_suite(void) {
    Suite *s = suite_create("Lexer");
    TCase *tc_sample = tcase_create("SplitTest");

    tcase_add_test(tc_sample, split_empty_empty);
    tcase_add_test(tc_sample, split_oneNumber_oneNumberToken);
    tcase_add_test(tc_sample, split_oneAnd_oneAndToken);
    tcase_add_test(tc_sample, split_oneString_oneStringToken);
    //tcase_add_test(tc_sample, split_threeToken);
    tcase_add_test(tc_sample, split_out);
    tcase_add_test(tc_sample, split_spaces);
    tcase_add_test(tc_sample, split_number);
    tcase_add_test(tc_sample, split_variable);
    tcase_add_test(tc_sample, split_variable2);

    suite_add_tcase(s, tc_sample);

	return s;
}