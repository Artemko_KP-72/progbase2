#include <assert.h>
#include <ctype.h>
#include <lexer.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <stringbuffer.h>

char *const UNKNOWN = "unknown";
char *const ASSIGN = "=";
char *const EQUAL = "==";
char *const NOT_EQUAL = "!=";
char *const M_EQUAL = "<=";
char *const L_EQUAL = ">=";
char *const PLUS = "+";
char *const MINUS = "-";
// char *const NUMBER "=";
char *const MULT = "*";
char *const DIV = "/";
char *const MORE = ">";
char *const LESS = "<";
char *const AND = "&&";
char *const OR = "||";
char *const NOT = "!";
char *const LPAR = "(";
char *const RPAR = ")";
char *const HEADBLOCK = "{";
char *const TAILBLOCK = "}";
char *const LMRACK = "[";
char *const RMRACK = "]";
char *const TERMINAL = "\n";
char *const LET = "let";
char *const WHILE = "while";
char *const IF = "if";
char *const ELSE = "else";
// char *const STRING
char *const COMMA = ",";
char *const BOOL_EXPR_TRUE = "true";
char *const BOOL_EXPR_FALSE = "false";
char *const POWER = "**";

char *const tokenToStr[] = {
    "unknown", "=",  "==",   "!=", "<=",   ">=",    "+",    "-",
    "*",       "/",  ">",    "<",  "%",    "&&",    "||",   "!",
    "(",       ")",  "{",    "}",  "[",    "]",     "\n",   "let",
    "while",   "if", "else", ",",  "true", "false", "out", "in" , "power"};

void Token_free(Token *self) { free(self); }

static void rec_free_tokens(LNode *copy) {
  if (copy->next != NULL) {
    rec_free_tokens(copy->next);
  }
  if (copy->value.type == TokenType_NUMBER ||
      copy->value.type == TokenType_STRING || copy->value.type == TokenType_VAR)
    free(copy->value.name);
}

char *TokenType_toString(TokenType token) {
  char *c = malloc(sizeof(char) * 10);
  // todo
  return c;
}

void Lexer_clearTokens(List *self) {
  if (self == NULL)
    return;
  if (self->head != NULL)
    rec_free_tokens(self->head);
  // free(self);
  // free(head->value.name);
}

void TokenType_print(TokenType type) { printf(" [%s ", tokenToStr[type]); }

bool Token_equals(Token *self, Token *other) {
  if (self == NULL && other == NULL)
    return true;
  if (self == NULL || other == NULL)
    return false;
  if (self->type == other->type) {
    if (self->name != NULL && other->name != NULL) {
      if (!strcmp(self->name, other->name)) {
        return true;
      }
    } else {
      if (self->name == NULL && other->name == NULL)
        return true;
    }
  }
  return false;
}

/*
void Lexer_printTokens(List *tokens) {
  Iterator * begin = ListNewBeginIterator(tokens);
  Iterator * end = ListNewBeginIterator(tokens);

  while (!Iterator_equals(begin, end)) {
    Token * token = Iterator_value(begin);
    if (tokens->value == TokenType_NUMBER) printf("<NUM,%s>", token->value)
    else if (tokens->value == TokenType_ID) printf("<ID,%s>", token->value)
    else if (tokens->value == TokenType_STRING) printf("<STR,%s>", token->value)
  }
}
*/
void List_print(List *self) {
  if (self == NULL) {
    return;
  }
  LNode *head = self->head;
  while (head != NULL) {
    Token *token = &head->value;
    if (token->type == TokenType_VAR)
      printf("<VAR, %s>  ", token->name);
    else if (token->type == TokenType_NUMBER)
      printf("<NUM, %s> ", token->name);
    else if (token->type == TokenType_TERMINAL)
      printf("<\\n> \n");
    else
      printf("<%s> ", token->name);
    head = head->next;
  }
}

//
//
//
//
//
//
//
//

int Lexer_splitTokens(List *tokens, const char *input) {
  if (input == NULL || tokens == NULL) {
    puts("NULL POINTER EXEPTION");
    return 1;
  }

  for (int i = 0; i < strlen(input); i++) {
    char *temp = malloc(sizeof(char) * 15);
    temp[0] = '\0';
    if (input[i] == '\0' || input[i] == ' ') {
    } else if (input[i] == '(')
      List_add(tokens, TokenType_LPAR, tokenToStr[TokenType_LPAR]);
    else if (input[i] == ')')
      List_add(tokens, TokenType_RPAR, tokenToStr[TokenType_RPAR]);
    else if (input[i] == ',')
      List_add(tokens, TokenType_COMMA, tokenToStr[TokenType_COMMA]);
    else if (input[i] == '+')
      List_add(tokens, TokenType_PLUS, tokenToStr[TokenType_PLUS]);
    else if (input[i] == '-')
      List_add(tokens, TokenType_MINUS, tokenToStr[TokenType_MINUS]);
    else if (input[i] == '*')
      List_add(tokens, TokenType_MULT, tokenToStr[TokenType_MULT]);
    else if (input[i] == '/')
      List_add(tokens, TokenType_DIV, tokenToStr[TokenType_DIV]);
    else if (input[i] == '{')
      List_add(tokens, TokenType_HEADBLOCK, tokenToStr[TokenType_HEADBLOCK]);
    else if (input[i] == '}')
      List_add(tokens, TokenType_TAILBLOCK, tokenToStr[TokenType_TAILBLOCK]);
    else if (input[i] == '[')
      List_add(tokens, TokenType_LBRACKET, tokenToStr[TokenType_LBRACKET]);
    else if (input[i] == ']')
      List_add(tokens, TokenType_RBRACKET, tokenToStr[TokenType_RBRACKET]);
    else if (input[i] == '\n')
      List_add(tokens, TokenType_TERMINAL, tokenToStr[TokenType_TERMINAL]);
    else if (input[i] == '=') {
      if (input[i + 1] == '=') {
        List_add(tokens, TokenType_EQUAL, tokenToStr[TokenType_EQUAL]);
        input++;
      } else
        List_add(tokens, TokenType_ASSIGN, tokenToStr[TokenType_ASSIGN]);
    } else if (input[i] == '!') {
      if (*(input + 1) == '=') {
        List_add(tokens, TokenType_NOT_EQUAL, tokenToStr[TokenType_NOT_EQUAL]);
        input++;
      } else
        List_add(tokens, TokenType_NOT, tokenToStr[TokenType_NOT]);
    } else if (input[i] == '<') {
      if (*(input + 1) == '=') {
        List_add(tokens, TokenType_L_EQUAL, tokenToStr[TokenType_L_EQUAL]);
        input++;
      } else
        List_add(tokens, TokenType_LESS, tokenToStr[TokenType_LESS]);
    } else if (input[i] == '>') {
      if (input[i + 1] == '=') {
        List_add(tokens, TokenType_M_EQUAL, tokenToStr[TokenType_M_EQUAL]);
        input++;
      } else
        List_add(tokens, TokenType_MORE, tokenToStr[TokenType_MORE]);
    } else if (input[i] == '&') {
      if (input[i + 1] != '&') {
        printf("\nWarning incorrect symbol '&' at %i", i);
        //@todo free
        return 1;
      } else {
        List_add(tokens, TokenType_AND, tokenToStr[TokenType_AND]);
        input++;
      }
    } else if (input[i] == '|') {
      if (input[i + 1] != '|') {
        printf("\nWarning incorrect symbol '|' at %i", i);
        //@todo free
        return 1;
      } else {
        List_add(tokens, TokenType_OR, tokenToStr[TokenType_OR]);
        input++;
      }
    } else if (input[i] == 'l' && input[i + 1] == 'e' && input[i + 2] == 't') {
      List_add(tokens, TokenType_LET, tokenToStr[TokenType_LET]);
      i += 3;
    } else if (input[i] == 'i' && input[i + 1] == 'f') {
      List_add(tokens, TokenType_IF, tokenToStr[TokenType_IF]);
      i++;
    } else if (input[i] == 'e' && input[i + 1] == 'l' && input[i + 2] == 's' &&
               input[i + 3] == 'e') {
      List_add(tokens, TokenType_ELSE, tokenToStr[TokenType_ELSE]);
      i += 3;
    } else if (input[i] == 't' && input[i + 1] == 'r' && input[i + 2] == 'u' &&
               input[i + 3] == 'e') {
      List_add(tokens, TokenType_BOOL_EXPR_TRUE, tokenToStr[TokenType_BOOL_EXPR_TRUE]);
      i += 4;
    } else if (input[i] == 'o' && input[i + 1] == 'u' && input[i + 2] == 't') {
      List_add(tokens, TokenType_OUT, tokenToStr[TokenType_OUT]);
      i += 2;
    } else if (input[i] == 'i' && input[i + 1] == 'n') {
      List_add(tokens, TokenType_IN, tokenToStr[TokenType_IN]);
      i += 1;
    } else if (input[i] == 'f' && input[i + 1] == 'a' && input[i + 2] == 'l' &&
               input[i + 3] == 's' && input[i + 4] == 'e') {
      List_add(tokens, TokenType_BOOL_EXPR_FALSE, tokenToStr[TokenType_BOOL_EXPR_FALSE]);
      input += 5;
    } else if (input[i] == '"') {
      i++;
      temp[0] = '"';
      int j = 1;
      while (input[i] != '"') {
        temp[j] = input[i];
        j++;
        i++;
        if (input[i] == '\n' || input[i] == '\0') {
          printf("invalid string operator at the %i", i);
          //@todo free
          return 1;
        }
      }
      temp[j] = '"';
      temp[j + 1] = '\0';
      List_add(tokens, TokenType_STRING, temp);
    } else if (isdigit(input[i])) {
      int j = 0;
      while (isdigit(input[i]) || input[i] == '.') {
        temp[j] = input[i];
        i++;
        j++;
      }
      temp[j] = '\0';
      i--;
      List_add(tokens, TokenType_NUMBER, temp);
    } else if (isalpha(input[i])) {
      for (int j = 0; j < 10 && isalpha(input[i]); j++) {
        temp[j] = input[i];
        temp[j + 1] = '\0';
        i++;
      }
      i--;
      if (input[i] != ' ') {
        printf("Error invalid variable %s", temp);
        return 1;
      }
      List_add(tokens, TokenType_VAR, temp);
    }
    if (temp[0] == '\0')
      free(temp);
  }

  // puts("NULL POINTER EXEPTION");
  return 0;
}

Token * Token_new(TokenType type, const char * name) {
  Token * curr = malloc(sizeof(Token));
  curr->type = type;
  curr->name = name;
  return curr;
}
//     /*
//     if (strcmp(head->value.name, tokenToStr[TokenType_TERMINAL] )) {
//       //TokenType_print(head->value.type);
//   //printf(" [%s ", tokenToStr[type]);
//       printf("[%s , %s]",tokenToStr[head->value.type] , head->value.name);
//     } else
//       printf("[\\n , %s] ", head->value.name);
//     head = head->next;
//   }
//   */
// }
// }

// // Atention impotent function
// // Atention impotent function
// // Atention impotent function
// // Atention impotent function
// // Atention impotent function
// // Atention impotent function
// // Atention impotent function
// // Atention impotent function
// /*
// int Lexer_splitTokens(const char *input, List *tokens) {
//   if (input == NULL || tokens == NULL) {
//     puts("NULL POINTER EXEPTION");
//     return 1;
//   }
//   if (strlen(input) == 0) {
//     puts("ATTENTION: ZERO LENGTH");
//     return 0;
//   }

//   for (int i = 0; i < strlen(input) && input[i] != '\0' ; i++) {
//     char * temp = malloc(sizeof(char) * 15);
//     temp[0] = '\0';
//     switch (input[i]) {
//     case ' ':
//       break;
//     case '+':
//       List_add(tokens, TokenType_PLUS, tokenToStr[TokenType_PLUS]);
//       break;

//     case '-':
//       List_add(tokens, TokenType_MINUS, tokenToStr[TokenType_MINUS]);
//       break;

//     case '*':
//       if (input[i + 1] == '*') {
//         List_add(tokens, TokenType_POWER, tokenToStr[TokenType_POWER]);
//         i++;
//       }
//       else
//         List_add(tokens, TokenType_MULT, tokenToStr[TokenType_MULT]);
//       break;
//     //
//     case '=':
//       if (input[i + 1] == '=') {
//         List_add(tokens, TokenType_ASSIGN, tokenToStr[TokenType_ASSIGN]);
//         i++;
//       }
//       else
//         List_add(tokens, TokenType_EQUAL, tokenToStr[TokenType_EQUAL]);
//       break;

//     case '<':
//       if (input[i + 1] == '=') {
//         List_add(tokens, TokenType_L_EQUAL, tokenToStr[TokenType_L_EQUAL]);
//         i++;
//       }
//       else
//         List_add(tokens, TokenType_LESS, tokenToStr[TokenType_LESS]);
//       break;

//     case '>':
//       if (input[i + 1] == '=') {
//         List_add(tokens, TokenType_M_EQUAL, tokenToStr[TokenType_M_EQUAL]);
//         i++;
//       }
//       else
//         List_add(tokens, TokenType_MORE, tokenToStr[TokenType_MORE]);
//       break;

//     case '|':
//       if (input[i + 1] == '|') {
//         List_add(tokens, TokenType_OR, tokenToStr[TokenType_OR]);
//         i++;
//       } else {
//         printf("Unknown command on %i\n", i);
//         free(temp);
//         return 1;
//       }

//     case '!':
//       if (input[i + 1] == '=') {
//         List_add(tokens, TokenType_NOT_EQUAL,
//         tokenToStr[TokenType_NOT_EQUAL]);
//         i++;
//       } else
//         List_add(tokens, TokenType_NOT, tokenToStr[TokenType_NOT]);
//       break;

//     case '\n':
//       List_add(tokens, TokenType_TERMINAL, tokenToStr[TokenType_TERMINAL]);
//       i++;
//       break;

//     case '&':
//       if (input[i + 1] == '&') {
//         List_add(tokens, TokenType_AND, tokenToStr[TokenType_AND]);
//         i++;
//       } else {
//         printf("Unknown command on %i\n", i);
//         free(temp);
//         return 1;
//       }
//       break;

//     case '/':
//       List_add(tokens, TokenType_DIV, tokenToStr[TokenType_DIV]);
//       break;

//     case '0':
//     case '1':
//     case '2':
//     case '3':
//     case '4':
//     case '5':
//     case '6':
//     case '7':
//     case '8':
//     case '9':
//       if (isdigit(input[i])) {
//         for (int j = 0; j < 10 && j < strlen(input) && (isdigit(input[i]) ||
//         input[i] == '.'); j++) {
//           temp[j] = input[i];
//           temp[j + 1] = '\0';
//           i++;
//         }
//         i--;
//       }
//       List_add(tokens, TokenType_NUMBER, temp);
//       break;

//     case '(':
//       List_add(tokens, TokenType_LPAR, "(\0");
//       break;

//     case ')':
//       List_add(tokens, TokenType_RPAR, ")\0");
//       break;

//     case '[':
//       List_add(tokens, TokenType_LPAR, "[\0");
//       break;

//     case ']':
//       List_add(tokens, TokenType_RPAR, "]\0");
//       break;

//     case '{':
//       List_add(tokens, TokenType_LPAR, "{\0");
//       break;

//     case '}':
//       List_add(tokens, TokenType_RPAR, "}\0");
//       break;

//     default:
//       if (isalpha(input[i])) {

//         if (input[i] == 'w' && input[i + 1] == 'h' && input[i + 2] == 'i' &&
//             input[i + 3] == 'l' && input[i + 4] == 'e') {
//           for (int j = 0; j < 5; j++) {
//             temp[j] = input[i];
//             temp[j + 1] = '\0';
//             i++;
//           }
//           i--;
//           List_add(tokens, TokenType_WHILE, tokenToStr[TokenType_WHILE]);
//           break;
//         }
// /*
//         if (input[i] == 'c' && input[i + 1] == 'o' && input[i + 2] == 'u' &&
//             input[i + 3] == 'n' && input[i + 4] == 't') {
//           for (int j = 0; j < 5; j++) {
//             temp[j] = input[i];
//             temp[j + 1] = '\0';
//             i++;
//           }
//           i--;
//           List_add(tokens, TokenType_COUNT, temp);
//           break;
//         }

//         if (input[i] == 'p' && input[i + 1] == 'u' && input[i + 2] == 's' &&
//             input[i + 3] == 'h') {
//           for (int j = 0; j < 4; j++) {
//             temp[j] = input[i];
//             temp[j + 1] = '\0';
//             i++;
//           }
//           i--;
//           List_add(tokens, TokenType_FUNCTION, temp);
//           break;
//         }

//         if (input[i] == 'a' && input[i + 1] == 't') {
//           for (int j = 0; j < 2; j++) {
//             temp[j] = input[i];
//             temp[j + 1] = '\0';
//             i++;
//           }
//           i--;
//           List_add(tokens, TokenType_FUNCTION, temp);
//           break;
//         }
//         if (input[i] == 'i') {
//           if (input[i + 1] == 'f') {
//             temp[0] = input[i];
//             temp[1] = input[i + 1];
//             temp[2] = '\0';
//             i++;
//             List_add(tokens, TokenType_LOOP, temp);
//             break;
//           }
//           if (input[i + 1] == 'n') {
//             temp[0] = input[i];
//             temp[1] = input[i + 1];
//             temp[2] = '\0';
//             i++;
//             List_add(tokens, TokenType_FUNCTION, temp);
//             break;
//           }
//         }

//         if (input[i] == 'n' && input[i + 1] == 'u' && input[i + 2] == 'm' &&
//             input[i + 3] == 't' && input[i + 4] == 'o' && input[i + 5] == 's'
//             &&
//             input[i + 6] == 't' && input[i + 7] == 'r') {
//           for (int j = 0; j < 8; j++) {
//             temp[j] = input[i];
//             temp[j + 1] = '\0';
//             i++;
//           }
//           i--;
//           List_add(tokens, TokenType_FUNCTION, temp);
//           break;
//         }

//         if (input[i] == 'o' && input[i + 1] == 'u' && input[i + 2] == 't') {
//           for (int j = 0; j < 3; j++) {
//             temp[j] = input[i];
//             temp[j + 1] = '\0';
//             i++;
//           }
//           i--;
//           List_add(tokens, TokenType_FUNCTION, temp);
//           break;
//         }

//         if (input[i] == 's') {
//           if (input[i + 1] == 't' && input[i + 2] == 'r') {
//             if (input[i + 3] == 'l' && input[i + 4] == 'e' &&
//                 input[i + 5] == 'n') {
//               for (int j = 0; j < 6; j++) {
//                 temp[j] = input[i];
//                 temp[j + 1] = '\0';
//                 i++;
//               }
//               i--;
//               List_add(tokens, TokenType_FUNCTION, temp);
//               break;
//             }
//             if (input[i + 3] == 't' && input[i + 4] == 'o' &&
//                 input[i + 5] == 'n' && input[i + 6] == 'u' &&
//                 input[i + 7] == 'm') {
//               for (int j = 0; j < 8; j++) {
//                 temp[j] = input[i];
//                 temp[j + 1] = '\0';
//                 i++;
//               }
//               i--;
//               List_add(tokens, TokenType_FUNCTION, temp);
//               break;
//             }
//           }
//           if (input[i + 1] == 'u' && input[i + 2] == 'b' &&
//               input[i + 3] == 's' && input[i + 4] == 't' &&
//               input[i + 5] == 'r') {
//             for (int j = 0; j < 6; j++) {
//               temp[j] = input[i];
//               temp[j + 1] = '\0';
//               i++;
//             }
//             i--;
//             List_add(tokens, TokenType_FUNCTION, temp);
//             break;
//           }

//           if (input[i + 1] == 'q' && input[i + 2] == 'r' &&
//               input[i + 3] == 't') {
//             temp[0] = input[i];
//             temp[1] = input[i + 1];
//             temp[2] = input[i + 2];
//             temp[3] = input[i + 3];
//             temp[4] = '\0';
//             i += 3;
//             List_add(tokens, TokenType_FUNCTION, temp);
//             break;
//           }
//         }*/
//         for (int j = 0; j < 10 && isalpha(input[i]); j++) {
//           temp[j] = input[i];
//           temp[j + 1] = '\0';
//           i++;
//         }
//         i--;
//         List_add(tokens, TokenType_VAR, temp);
//         break;
//       }
//     }
//     if(temp[0] == '\0')
//       free(temp);
//   }
//   return 0;
// }