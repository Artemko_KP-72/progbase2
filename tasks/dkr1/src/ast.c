#include <ast.h>

AstNode * AstNode_new(AstNodeType type, const char * name) {
    AstNode * self = malloc(sizeof(AstNode));
    self->type = type;
    self->name = name;
    return self;
}

void AstNode_free(AstNode * self) {
    free(self);
}
