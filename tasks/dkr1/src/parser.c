#include <parser.h>
#include <list.h>
#include <assert.h>
#include <stdbool.h>
#include <lexer.h>
#include <stdio.h>
#include <stdlib.h>
#include <ast.h>
#include <string.h>

typedef struct {
    Iterator * tokens;
    Iterator * tokensEnd;
    char * error;
    int level;
} Parser;

static Tree * accept(Parser * self, TokenType tokenType);
static Tree * expect(Parser * self, TokenType tokenType); 

static Tree * prog       (Parser * parser);
static Tree * var_decl   (Parser * parser);
static Tree * st         (Parser * parser);
static Tree * ID         (Parser * parser);
static Tree * expr       (Parser * parser);
static Tree * expr_st    (Parser * parser);
static Tree * block_st   (Parser * parser);
static Tree * select_st  (Parser * parser);
static Tree * iter_st    (Parser * parser);
static Tree * assign     (Parser * parser);
static Tree * assign_ap  (Parser * parser);
static Tree * log_or     (Parser * parser);
static Tree * log_or_ap  (Parser * parser);
static Tree * log_and    (Parser * parser);
static Tree * log_and_ap (Parser * parser);
static Tree * eq         (Parser * parser);
static Tree * eq_ap      (Parser * parser);
static Tree * rel        (Parser * parser);
static Tree * rel_ap     (Parser * parser);
static Tree * add        (Parser * parser);
static Tree * add_ap     (Parser * parser);
static Tree * mult       (Parser * parser);
static Tree * mult_ap    (Parser * parser);
static Tree * unary      (Parser * parser);
static Tree * primary    (Parser * parser);
static Tree * NUMBER     (Parser * parser);
static Tree * STRING     (Parser * parser);
static Tree * var_or_call(Parser * parser);
static Tree * parentheses(Parser * parser);
static Tree * fn_call    (Parser * parser);
static Tree * arg_list   (Parser * parser);

static char *strdup(const char *str) {
    char *dup = malloc(strlen(str) + 1);
    if (dup == NULL)
        Error("Not enougf memory to create string");
    strcpy(dup, str);
    return dup;
}

/*
    Hz how it works
*/
static void StringBuffer_appendFormat(char *self, const char *fmt, ...)
{
    va_list vlist;
    va_start(vlist, fmt);
    size_t bufSize = vsnprintf(NULL, 0, fmt, vlist);
    char * buf = malloc(bufSize + 1);
    va_start(vlist, fmt);
    vsnprintf(buf, bufSize + 1, fmt, vlist);
    va_end(vlist);
    StringBuffer_append(self, buf);
    free(buf);
}

Tree * Parser_buildNewAstTree(List * tokens) {
    Parser parser = {
        .tokens = List_getNewBeginIterator(tokens),
        .tokensEnd = List_getNewEndIterator(tokens),
        .error = NULL,
        .level = -1
    };
    Tree * progNode = prog(&parser);
    if (parser.error)
        Error(parser.error);
    else if (!Iterator_equals(parser.tokens, parser.tokensEnd))
        Error(parser.error);
    Iterator_free(parser.tokens);
    Iterator_free(parser.tokensEnd);
    return progNode;
}

static bool eoi(Parser * self) {
    return Iterator_equals(self->tokens, self->tokensEnd);
}

#define TRACE_CALL(P) \
    P->level++; \
    Parser * _parcerPt __attribute__((cleanup(Parser_decLevel))) = P;

// trace(P, __func__);

void Parser_decLevel(Parser ** parserPt) {
    (*parserPt)->level--;
}

#define TRACE_EXPECT(P, TYPE) \
;
    //traceExpect(P, TYPE);

static void traceLevel(int level) {
    for (int i = 0; i < level; i++) {
        putchar('.');
        putchar('.');
    }
}

static void trace(Parser * parser, const char * fn) {
    traceLevel(parser->level);
    if (eoi(parser)) {
        printf("%s: <EOI>\n", fn);
        return;
    }
    Token * token = Iterator_value(parser->tokens);
    TokenType type = token->type;
    if( type == TokenType_NUMBER || type == TokenType_STRING || type == TokenType_VAR)
        printf("%s: <%s, %s>\n", fn, TokenType_toString(type), token->name);
    else
        printf("%s: <%s>\n", fn, TokenType_toString(type));
}

static void traceExpect(Parser * parser, TokenType expectedType) {
    traceLevel(parser->level);
    printf("<%s>\n", TokenType_toString(expectedType));
}

//////////

static AstNodeType TokenType_toAstNodeType(TokenType type) {
        if (type == TokenType_ASSIGN) return AstNodeType_ASSIGN;
        else if (type == TokenType_EQUAL) return AstNodeType_EQ;
        else if (type == TokenType_NOT_EQUAL) return AstNodeType_NEQ;
        else if (type == TokenType_NOT) return AstNodeType_NOT;
        else if (type == TokenType_LESS) return AstNodeType_LESS;
        else if (type == TokenType_MORE) return AstNodeType_MORE;
        else if (type == TokenType_L_EQUAL) return AstNodeType_LEQ;
        else if (type == TokenType_M_EQUAL) return AstNodeType_MEQ;
        else if (type == TokenType_PLUS) return AstNodeType_ADD;
        else if (type == TokenType_MINUS) return AstNodeType_SUB;
        else if (type == TokenType_MULT) return AstNodeType_MUL;
        else if (type == TokenType_DIV) return AstNodeType_DIV;
        else if (type == TokenType_MOD) return AstNodeType_MOD;
        else if (type == TokenType_AND) return AstNodeType_AND;
        else if (type == TokenType_OR) return AstNodeType_OR;
        else if (type == TokenType_NUMBER) return AstNodeType_NUMBER;
        else if (type == TokenType_STRING) return AstNodeType_STRING;
        else if (type == TokenType_VAR) return AstNodeType_ID;
        // @todo to finish up ...
        else return AstNodeType_UNKNOWN;
    }
}

static Tree * accept(Parser * self, TokenType tokenType) {
    if (eoi(self)) return false;
    Token * token = Iterator_value(self->tokens);
    if (token->type == tokenType) {
        AstNodeType astType = TokenType_toAstNodeType(tokenType);
        char * astName = strdup((char *)token->name);
        AstNode * node = AstNode_new(astType, astName);
        Tree * tree = Tree_new(node);
        Iterator_next(self->tokens);
        return tree;
    }
    return NULL;
}

static Tree * expect(Parser * parser, TokenType tokenType) {
    Tree * tree = accept(parser, tokenType);
    if(tree != NULL) {
        TRACE_EXPECT(parser, tokenType);
        return tree;
    }
        const char * currentTokenType = eoi(parser) 
        ? "end of input"
        : TokenType_toString(((Token *)Iterator_value(parser->tokens))->type);
        // @todo something
        return NULL;
}

typedef Tree * (*GrammarRule)(Parser * parser);

static bool ebnf_multiple(Parser * parser, List * nodes, GrammarRule rule) {
    Tree * node = NULL;
    while((node = rule(parser)) && !parser->error) {
        List_add(nodes, node->type, node->name);
    }
    return parser->error == NULL ? true : false;
}

static Tree * ebnf_select(Parser * parser, GrammarRule rules[], size_t rulesLen) {
    Tree * node = NULL;
    for (int i = 0; i < rulesLen && !node; i++) {
        GrammarRule rule = rules[i];
        node = rule(parser);
        if (parser->error) return NULL;
    }
    return node;
}

static Tree * ebnf_selectLexemes (Parser * parser, TokenType types[], size_t typesLen) {
    Tree * node = NULL;
    for (int i = 0; i < typesLen && !node; i++) {
        node = accept(parser, types[i]);
    }
    return node;
}

static Tree * ebnf_ap_main_rule(Parser * parser, GrammarRule next, GrammarRule ap) {
    Tree * nextNode = next(parser);
    if (nextNode) {
        Tree * apNode = ap(parser);
        if (apNode) {
            List_insert(apNode->children, 0,  nextNode->tokens, nextNode->name);
            return apNode;
        }
        return nextNode;
    }
    return NULL;
}

static Tree * ebnf_ap_recursive_rule(Parser * parser, TokenType types[], size_t typesLen, GrammarRule next, GrammarRule ap) {
    Tree * opNode = ebnf_selectLexemes(parser, types, typesLen);
    if (opNode == NULL) return NULL;
    Tree * node = NULL;
    Tree * nextNode = next(parser);
    Tree * apNode = ap(parser);
    if (apNode) {
        List_insert(apNode->children, nextNode, 0);
        node = apNode;
    }
    else {
        node = nextNode;
    }
    List_add(opNode->children, node);
    return opNode;
}

static Tree * ID        (Parser * parser) {
    TRACE_CALL(parser);
    return accept(parser, TokenType_VAR);
}

static Tree * NUMBER     (Parser * parser) {
    TRACE_CALL(parser);
    return accept(parser, TokenType_NUMBER);
}

static Tree * STRING     (Parser * parser) {
    TRACE_CALL(parser);
    return accept(parser, TokenType_STRING);
}

static Tree * ID_expect(Parser * parser) {
    return expect(parser, TokenType_VAR);
}

static Tree * NUMBER_expect(Parser * parser) {
    return expect(parser, TokenType_NUMBER);
}

static Tree * STRING_expect(Parser * parser) {
    return expect(parser, TokenType_STRING);
}

static Tree * prog      (Parser * parser) {
    TRACE_CALL(parser);
    Tree * progNode = Tree_new(AstNode_new(AstNodeType_PROGRAM, "program"));
    (void)ebnf_multiple(parser, progNode->children, var_decl);
    (void)ebnf_multiple(parser, progNode->children, st);
    return progNode;
}

static Tree * var_decl  (Parser * parser) {
    TRACE_CALL(parser);
    if (!accept(parser, TokenType_LET))
        Error("declaration of variable is missing");
    Tree * idNode = ID_expect(parser);
    if (!idNode)
        Error("Variable missing");
    if (!expect(parser, TokenType_ASSIGN))
        Error(" '=' is missing");
    Tree * exprNode = expr(parser);
    if (!exprNode) 
        Error("Expression missing");
    if (!expect(parser, TokenType_TERMINAL)) 
        Error("Expected \\n");
    Tree * varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR, "declareVar"));
    List_add(varDecl->children, idNode);
    List_add(varDecl->children, exprNode);
    return varDecl;
}

static Tree * st        (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_select(parser, 
    (GrammarRule[]) {
        block_st,
        select_st,
        iter_st,
        expr_st
    }, 4);
}

static Tree * expr      (Parser * parser) {
    TRACE_CALL(parser);
    return assign(parser);
}

static Tree * expr_st   (Parser * parser) {
    TRACE_CALL(parser);
    Tree * exprNode = expr(parser);
    if (exprNode)
        expect(parser, TokenType_TERMINAL);
    else 
        accept(parser, TokenType_TERMINAL);
    return exprNode;
}

static Tree * block_st  (Parser * parser) {
    TRACE_CALL(parser);
    if (!accept(parser, TokenType_HEADBLOCK)) 
        Error("Missing '{'");
    Tree * blockNode = Tree_new(AstNode_new(AstNodeType_BLOCK, "block"));
    if (ebnf_multiple(parser, blockNode->children, st))
        (void)expect(parser, TokenType_TAILBLOCK);
    return blockNode;
}

static Tree * select_st (Parser * parser) {
    TRACE_CALL(parser);
    if (!accept(parser, TokenType_IF))
        Error("IF struction is not correctly used");
    if (!expect(parser, TokenType_LPAR))
        Error("Expected ')' ");
    Tree * exprNode = expr(parser);
    if (!exprNode)
        Error("Missed expretion");
    if (!expect(parser, TokenType_RPAR))
        // @todo free possible memory leak
        Error("Missed expretion ')' ");
    Tree * stNode = st(parser);
    if (stNode == NULL) 
        // @todo free possible memory leak
        Error("Missed expretion ')' ");
    Tree * ifNode = Tree_new(AstNode_new(AstNodeType_IF, "if"));
    List_add(ifNode->children, exprNode);
    List_add(ifNode->children, stNode);

    if(accept(parser, TokenType_ELSE)) {
        Tree * elseNode = st(parser);
        if (elseNode == NULL)
            // @todo free possible memory leak
            Error("Missed expretion ')' ");
        if (parser->error)
            // @todo free possible memory leak
            Error(parser->error);
        List_add(ifNode->children, elseNode);
    }

    return ifNode;
}

static Tree * iter_st   (Parser * parser) {
    TRACE_CALL(parser);
    if (!accept(parser, TokenType_WHILE))
        Error("WHILE struction is not correctly used");
    if (!expect(parser, TokenType_LPAR))
    Error("Expected ')' ");
    Tree * exprNode = expr(parser);
    if (!exprNode)
        // @todo free possible memory leak
        Error("Missed expretion ");
    if (!expect(parser, TokenType_RPAR))
        // @todo free possible memory leak
        Error("Missed token ')' ");
    Tree * stNode = st(parser);
    if (stNode == NULL) 
        // @todo free possible memory leak
        Error("Missed statment ')' ");
    Tree * whileNode = Tree_new(AstNode_new(AstNodeType_WHILE, "while"));
    List_add(whileNode->children, exprNode);
    List_add(whileNode->children, stNode);
    return whileNode;
}

static Tree * assign     (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_main_rule(parser,log_or, assign_ap);
}

static Tree * assign_ap  (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_recursive_rule(parser, 
    (TokenType[]) {
        TokenType_ASSIGN
    }, 1, log_or, assign_ap);
}

static Tree * log_or     (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_main_rule(parser,log_and, log_or_ap);
}

static Tree * log_or_ap  (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_recursive_rule(parser, 
    (TokenType[]) {
        TokenType_OR
    }, 1, log_and, log_or_ap);
}
static Tree * log_and    (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_main_rule(parser, eq, log_and_ap);
}
static Tree * log_and_ap (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_recursive_rule(parser, 
    (TokenType[]) {
        TokenType_AND
    }, 1, eq, log_and_ap);
}
static Tree * eq         (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_main_rule(parser, rel, eq_ap);
}
static Tree * eq_ap      (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_recursive_rule(parser, 
    (TokenType[]) {
        TokenType_EQUAL,
        TokenType_NOT_EQUAL
    }, 2, rel, eq_ap);
}
static Tree * rel        (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_main_rule(parser, add, rel_ap);
}
static Tree * rel_ap     (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_recursive_rule(parser, 
    (TokenType[]) {
        TokenType_LESS,
        TokenType_L_EQUAL,
        TokenType_MORE,
        TokenType_M_EQUAL
    }, 4, add, rel_ap);
}
static Tree * add        (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_main_rule(parser, mult, add_ap);
}
static Tree * add_ap     (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_recursive_rule(parser, 
    (TokenType[]) {
        TokenType_PLUS,
        TokenType_MINUS
    }, 2, mult, add_ap);
}
static Tree * mult       (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_main_rule(parser, unary, mult_ap);
}

static Tree * mult_ap    (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_ap_recursive_rule(parser, 
    (TokenType[]) {
        TokenType_MULT,
        TokenType_DIV,
        TokenType_MOD
    }, 3, unary, mult_ap);
}

static Tree * unary      (Parser * parser) {
    TRACE_CALL(parser);
    Tree * opNode = ebnf_selectLexemes(parser, 
    (TokenType[]) {
        TokenType_PLUS,
        TokenType_MINUS,
        TokenType_NOT
    }, 3);
    Tree * primNode = primary(parser);
    if (!primNode)
        Error("Primary token lost");
    if (opNode) {
        List_add(opNode->children, primNode);
        return opNode;
    }
    return primNode;
}

static Tree * primary    (Parser * parser) {
    TRACE_CALL(parser);
    return ebnf_select(parser, 
    (GrammarRule[]) {
        NUMBER,
        STRING,
        var_or_call,
        parentheses
    }, 4);
}
static Tree * var_or_call(Parser * parser) {
    TRACE_CALL(parser);
    Tree * varNode = ID(parser);
    Tree * argListNode = fn_call(parser);
    if (argListNode) {
        List_add(varNode->children, argListNode);
    }
    return varNode;
}

static Tree * parentheses(Parser * parser) {
    TRACE_CALL(parser);
    if (!accept(parser, TokenType_LPAR)) 
        Error("Token missing '(' ");;
    Tree * exprNode = expr(parser);
    if(!expect(parser, TokenType_RPAR)
        Error("Token missing ')' ");
    return exprNode;
}

static Tree * fn_call    (Parser * parser) {
    TRACE_CALL(parser);
    if (!accept(parser, TokenType_LPAR)) 
        Error("Token missing '(' ");
    Tree * argListNode = arg_list(parser);
    if(!expect(parser, TokenType_RPAR))
        Error("Token missing ')' "); 
    return argListNode;
}

static Tree * arg_list   (Parser * parser) {
    TRACE_CALL(parser);
    Tree * exprNode = expr(parser);
    Tree * argListNode = Tree_new(AstNode_new(AstNodeType_ARGLIST, "arglist"));
    if (exprNode) {
        List_add(argListNode->children, exprNode);
        while(true) {
            if(!accept(parser, TokenType_COMMA)) break;
            exprNode = expr(parser);
            if(exprNode) {
                List_add(argListNode->children, exprNode);
            }
            else {
                break;
            }
        }
    }
    return argListNode;
}
