#ifndef LIST
#include <list.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#endif

// if you have any quastion, write me on telegram 
// i will explain why i decided that way
// @matan_man
static LNode *LNode_new(TokenType type, char * name);
/*
static Token to_inform_error;
to_inform_error.type = TokenType_NUMBER;
to_inform_error.name = NULL;
*/
static LNode *LNode_new(TokenType type, char * name) {
  LNode *new = malloc(sizeof(LNode));

  if (new != NULL) {
    new->value.type = type;
    new->value.name = name;
    new->next = NULL;
  }
  return new;
}

List *List_new(void) {
  return malloc(sizeof(List));
}

void recursion_delite(LNode *copy) {
  if (copy->next != NULL)
    recursion_delite(copy->next);
  free(copy);
}

void List_free(List *self) {
  if (self == NULL)
    return;
  if (self->head != NULL)
    recursion_delite(self->head);
  free(self);
}

void List_add(List *self, TokenType type, char * name) {
  if (self == NULL) {
    return;
  }
  LNode *copyself = self->head;
  if (copyself != NULL) {
    while (copyself->next != NULL) {
      copyself = copyself->next;
    }
    copyself->next = LNode_new(type, name);
  } else {
    self->head = LNode_new(type, name);
  }
}

void List_insert(List *self, size_t index, TokenType type, char * name) {
  if (self == NULL) {
    return;
  }
  LNode *copyself = self->head;
  int counter = 1;
  while (copyself->next != NULL && counter != index) {
    copyself = copyself->next;
    counter++;
  }
  if (copyself->next != NULL) {
    // if 1 2 .. 3 4
    LNode *temp = copyself->next->next;
    copyself->next = LNode_new(type, name);
    copyself->next->next = temp;
  } else {
    // if 1 2 3 4 ..
    copyself->next = LNode_new(type, name);
  }
}

size_t List_count(List *self) {
  if (self == NULL) {
    return 0;
  }
  LNode *copyself = self->head;
  int count = 0;
  while (copyself != NULL) {
    copyself = copyself->next;
    count++;
  }
  return count;
}

Token * List_at(List *self, size_t index) {

  if (self == NULL)
    return NULL;
  LNode *copyself = self->head;
  int counter = 0;
  while (copyself != NULL) {
    if (counter == index)
      return &copyself->value;
    counter++;
    copyself = copyself->next;
  }
  return NULL;
}

int List_set(List *self, size_t index, TokenType type, char * name) {
  if (self == NULL)
    return 1;
  LNode *copyself = self->head;
  int counter = 1;
  while (copyself->next != NULL) {
    copyself = copyself->next;
    counter++;
    if (counter == index) {
      copyself->next->value.type = type;
      strcpy(copyself->next->value.name, name);
      return 0;
    }
  }
  return 1;
}

int List_removeAt(List *self, size_t index) {
  if (self == NULL || List_at(self, index)== NULL)
    return 1;
  if (index == 1) {
    LNode *tmp = self->head;
    self->head = self->head->next;
    free(tmp);
    return 0;
  }
  LNode *copyself = self->head;
  int counter = 2;
  while (copyself->next != NULL) {
    if (counter == index) {
      LNode *temp = copyself->next;
      copyself->next = copyself->next->next;
      free(temp);
      return 0;
    }
    copyself = copyself->next;
    counter++;
  }
  return 1;
}
/*

*/

//typedef __Iterator Iterator;

Iterator *  Iterator_new(List * head) {
  Iterator * self = malloc(sizeof(Iterator));
  self->head = head;
  if(head != NULL)
    self->node = head->head;
  else 
    self->node = NULL;
  return self;
}

Iterator * List_getNewBeginIterator (List * self) {
  return Iterator_new(self);
}

Iterator * List_getNewEndIterator (List * self) {
  Iterator * end = malloc(sizeof(Iterator));
  end->head = self;
  end->node = NULL;
  return end;
}

void Iterator_free(Iterator * self) {
  free(self);
}

void * Iterator_value  (Iterator * self) {
  if (self != NULL)
    return &self->node->value;
  else 
    return NULL;
}
void   Iterator_next     (Iterator * self) {
  self->node = self->node->next;
}

void   Iterator_prev     (Iterator * self) {
  LNode * prev = self->head->head;
  while (prev->next != self->node) {
    prev = prev->next;
  }
  self->node = prev;
}

void   Iterator_advance  (Iterator * self, IteratorDistance n) {
  for(int i = 0; i < n && self->node != NULL; i++) {
    self->node = self->node->next;
  }
}

bool   Iterator_equals   (Iterator * self, Iterator * other) {
  return self->head == other->head && self->node == other->node;
}

IteratorDistance Iterator_distance (Iterator * begin, Iterator * end) {
  long i = 0;
  LNode * curr = begin->node;
  while (curr != end->node) {
    curr = curr->next;
    i++;
  }
  return i;
}