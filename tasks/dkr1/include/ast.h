#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <list.h>

typedef enum {
    AstNodeType_UNKNOWN,
    AstNodeType_ASSIGN,
    AstNodeType_ADD,
    AstNodeType_SUB,
    AstNodeType_MUL,
    AstNodeType_DIV,
    AstNodeType_MOD,
    AstNodeType_EQ,
    AstNodeType_NEQ,
    AstNodeType_NOT,
    AstNodeType_MORE,
    AstNodeType_LESS,
    AstNodeType_MEQ,
    AstNodeType_LEQ,
    AstNodeType_AND,
    AstNodeType_OR,
    //
    AstNodeType_NUMBER,
    AstNodeType_STRING,
    AstNodeType_ID,
    //
    AstNodeType_ARGLIST,
    AstNodeType_BLOCK,
    AstNodeType_IF,
    AstNodeType_WHILE,
    AstNodeType_DECLAREVAR,
    AstNodeType_PROGRAM,
} AstNodeType;

typedef struct __AstNode AstNode;
struct __AstNode {
    AstNodeType type;
    const char * name;
};

AstNode * AstNode_new(AstNodeType type, const char * name);
void AstNode_free(AstNode * self);
