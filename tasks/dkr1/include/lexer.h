#pragma once 

#include <stdbool.h>
// to cheak )
#include "list.h"


char *  TokenType_toString(TokenType token);

 Token * Token_new(TokenType type, const char * name);
 void Token_free(Token * self);
 void TokenType_print(TokenType type);
 
 bool Token_equals(Token * self, Token * other);
 int Lexer_splitTokens(List * tokens, const char * input);
 void Lexer_clearTokens(List * tokens);
 
 void Lexer_printTokens(List * tokens);
