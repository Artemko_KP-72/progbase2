#pragma once
#include <stdlib.h>
#include <stdbool.h>

#define Error(MSG) do { \
  fprintf(stderr, "%s\n", MSG); \
  assert(0 && MSG); \
  abort(); \
} while(0)

typedef enum {
  TokenType_UNKNOWN,
  TokenType_ASSIGN,
  TokenType_EQUAL,
  TokenType_NOT_EQUAL,
  TokenType_L_EQUAL,
  TokenType_M_EQUAL,
  TokenType_PLUS,
  TokenType_MINUS,
  TokenType_MULT,
  TokenType_DIV,
  TokenType_LESS,
  TokenType_MORE,
  TokenType_MOD,
  TokenType_AND,
  TokenType_OR,
  TokenType_NOT,
  // ()
  TokenType_LPAR,
  TokenType_RPAR,
  // {}
  TokenType_HEADBLOCK,
  TokenType_TAILBLOCK,
  // []
  TokenType_LBRACKET,
  TokenType_RBRACKET,
  TokenType_TERMINAL,
  TokenType_LET,
  TokenType_WHILE,
  TokenType_IF,
  TokenType_ELSE,
  TokenType_COMMA,
  TokenType_BOOL_EXPR_TRUE,
  TokenType_BOOL_EXPR_FALSE,
  TokenType_OUT,
  TokenType_IN,
  TokenType_POWER,
  TokenType_NUMBER,
  TokenType_STRING,
  TokenType_VAR
} TokenType;

typedef struct __Token Token;
struct __Token {
  TokenType type;
  char * name;
};

// this is shamefull decision

typedef struct __List List;
typedef struct __LNode LNode;

struct __List {
    LNode *head;
  };
  
  struct __LNode {
    Token value;
    LNode *next;

  // when i understand that dkr need universal tree, 
  // i have allready realized first part so ...
  void ** extra;
  };

List *List_new(void);
void List_free(List *self);

void List_add(List *self, TokenType type, char * name);
void List_insert(List *self, size_t index,  TokenType type, char * name);
int List_removeAt(List *self, size_t index);
size_t List_count(List *self);
Token * List_at(List *self, size_t index);

/**
 *  @returns old value at index
 */
int List_set(List *self, size_t index, TokenType type, char * name);

void List_print(List *self);

// for iterator


typedef struct __Iterator Iterator;
typedef long IteratorDistance;

struct __Iterator {
    List * head;
    LNode * node;
  };
  
void Iterator_free(Iterator * self);

void * Iterator_value    (Iterator * self);
void   Iterator_next     (Iterator * self);
void   Iterator_prev     (Iterator * self);
void   Iterator_advance  (Iterator * self, IteratorDistance n);
bool   Iterator_equals   (Iterator * self, Iterator * other);
IteratorDistance Iterator_distance (Iterator * begin, Iterator * end);

Iterator * List_getNewBeginIterator (List * self);
Iterator * List_getNewEndIterator   (List * self);