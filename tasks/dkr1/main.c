#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <assert.h>
#include <stdio.h>

//#include <tree.h>
#include <list.h>
#include <lexer.h>
struct read {
    char srt[10000];

};

int fileExists(const char *fileName);
long getFileSize(const char *fileName);

int main(int argc, char* argv[]) {
	bool exist = false;
    char big_buffer[10000];
    if (argc > 1) {
        for (int i = 0; i < strlen(argv[1]) - 2; i++) {
            if (argv[1][i] == '.' && argv[1][i + 1] == 'j' && argv[1][i + 2] == 'l' && argv[1][i + 3] == 'l') {
                exist = true;
                break;
            }
        }
    } else {
        printf("Bad\n");
        return EXIT_FAILURE;
    }

    if (exist != true || !fileExists(argv[1])) {
		puts("Correct file not found");
		return EXIT_FAILURE; 
	}
    
    int nread = 0;
	char current[1000];
	int len = 0;
    FILE *f = fopen(argv[1], "r");
    fread(big_buffer, sizeof(char), 100000 ,f);

	List * tokens = List_new();
	// puts("before");
	// Lexer_splitTokens(tokens, " ");
	// puts("after");
	// puts("before");
	// Lexer_splitTokens(tokens, " ");
	// puts("after");
	// puts("before");
	// Lexer_splitTokens(tokens, " ");
	// puts("after");
    for (int i = 0; i < getFileSize(argv[1]); i += strlen(current)) {
		sscanf(big_buffer + i, "%[^\n]%*c %n", current, &nread);
		if (nread == 0)
			nread++;
		len = strlen(current);
		current[len] = '\n';
		current[len + 1] = '\0';
		printf("~%s", current);
		Lexer_splitTokens(tokens, current);
	}

	puts("\n==========\n");
	free(f);
	List_print(tokens);
	Lexer_clearTokens(tokens);
	List_free(tokens);
	puts("");
	return EXIT_SUCCESS;
}

int fileExists(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return false;  // false: not exists
    fclose(f);
    return true;  // true: exists
}

long getFileSize(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fclose(f);
    return fsize;
}
