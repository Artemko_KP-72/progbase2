#include <callback.h>

static void universal_foreach(int * arr, int start_arg, int target_to_reach , int increaser, void (*action)(int, int)) {
    for (int i = start_arg; i != target_to_reach; i+= increaser ) {
        action(arr[i], i);
    }
}


void Array_foreach(int arr[], int len, void (*action)(int, int)) {
    universal_foreach(arr, 0, len , 1, action);
}

void Array_foreachReversed(int arr[], int len, void (*action)(int, int)) {
    universal_foreach(arr, len - 1, 0 , -1, action);
}
