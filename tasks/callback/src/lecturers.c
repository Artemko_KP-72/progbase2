#include <lecturers.h>


int static lect_comparer(const void * x , const void * y);

void lectures_sorting(void) {
    lect * to_sort = malloc(sizeof(lect) * 4); 
     strcpy(to_sort[0].Name, "Viktor");  strcpy(to_sort[0].LastName ,"LegeZZa"); to_sort[0].lessons_per_week = 4;
     strcpy(to_sort[1].Name, "Olena");  strcpy(to_sort[1].LastName ,"Khomenko"); to_sort[1].lessons_per_week = 6;
     strcpy(to_sort[2].Name, "Ruslan");  strcpy(to_sort[2].LastName ,"Phadynyak"); to_sort[2].lessons_per_week = 3;
     strcpy(to_sort[3].Name, "Victoria");  strcpy(to_sort[3].LastName ,"Suschuk"); to_sort[3].lessons_per_week = 11;
    print(to_sort);
    printf("\n *** Describe the structure of the Teacher and using qsort (), \n"
    "perform the sorting of the elements array in the order of increasing the number of pairs per week.\n\n");
    qsort((void * )&to_sort[0], 4, sizeof(lect), lect_comparer);
    print(to_sort);
}

int static lect_comparer(const void * x , const void * y) {
    lect * a = (lect *)x;
    lect * b = (lect *)y;
    return - a->lessons_per_week + b->lessons_per_week;
}


void print(lect * l) {
for (int i = 0; i < 4; i++, l++) 
    printf("Name is %10s, Last Name is %10s, %3i lecturs per week \n", l->Name, l->LastName, l->lessons_per_week);
}