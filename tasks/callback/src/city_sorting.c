#include <city_sorting.h>


int static word_comparer(const void * a , const void * b);
bool static isLoud(char a);

bool static isLoud(char a) {
    switch(a) {
        case 'a':
        case 'o':
        case 'u':
        case 'y':
        case 'e':
        case 'i':        
        case 'A':
        case 'O':
        case 'U':
        case 'Y':
        case 'E':
        case 'I':
            return true;
        default:
            return false;
    }
}

int static word_comparer(const void * x , const void * y) {
    const char * a = x;
    const char * b = y;
    int counter = 0;
    for (int i = 0; a[i] != '\0'; i++) {
        if (!isLoud(a[i]))
            counter++;
    }

    for (int i = 0; b[i] != '\0'; i++) {
        if (!isLoud(b[i]))
            counter--;
    }
    return counter > 0 ? 0 : 1;
}

void print_cities(char * words) {
    for (int i = 0; i < AMOUNT; i++) {
        printf("%s ", words);
        words += WORD_SIZE;
    }
    puts("");
}

void simple_types_sorting(void) {
    char words[AMOUNT][WORD_SIZE] = {
        "Kiev",
        "Warsaw",
        "Rome",
        "Paris",
        "Minsk",
        "Vienna",
        "Sofia",
        "Odessa",
        "Madrid",
        "Saint Petersburg"
    };
    print_cities(words[0]);
    printf("\n *** Sort & output cityes in count of they 'prigolosnyh' letter, for example(Lesya) have 2 'prigolosnyh' letter 'L' & 's'\n\n");
    qsort((void * )&words[0], AMOUNT, sizeof(char) * WORD_SIZE, word_comparer);
    print_cities(words[0]);
    Console_getChar();
}