#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <progbase.h>
#include <progbase/console.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <callback.h>
#include <city_sorting.h>
#include <lecturers.h>

/*
gcc main.c -std=c11 -Wall -Werror -pedantic-errors -lm -lprogbase
*/

void menu(void);

static void output(int array_el, int index) {
  if (index % 2 == 0)
    printf("index = %i, value = %i\n", index, array_el);
}

int main(const int argc, const char *argv[]) {
  menu();
  return EXIT_SUCCESS;
}

void menu(void) {
  int arr[] = {0, 31, 72, 93, 37, 74, 62, 54, 12, 01, 9, 89};
  int switcher = 1;
  char key = '\0';
  int names_count = 4;
  char *names2[] = {"Simple types sorting", "Lecturers sorting",
                    "function with function argument", "exit"};
  for (int i = 0; i < names_count; i++) {
  }
  Console_hideCursor();

  while (key != 'E') {

    Console_clear();

    puts("Welcome to my program\n");
    for (int i = 0; i < names_count; i++) {
      if (switcher == i + 1)
        Console_setCursorAttribute(FG_YELLOW);
      printf("%s\n", names2[i]);

      Console_setCursorAttribute(FG_DEFAULT);
    }

    key = Console_getChar();

    if (key == 10) {
      Console_clear();
      if (switcher == 1) {
        simple_types_sorting();
      }

      if (switcher == 2) {
        lectures_sorting();
        Console_getChar();
      }

      if (switcher == 3) {
        Array_foreachReversed(arr, 12, output);
        // Array_foreach(arr, 12, output);
        printf("\n *** У функції main() створити і вивести тестовий масив цілих чисел\n "
               "та викликати функції Array_foreach() та Array_foreachReversed \n"
               "на тестовому масиві чисел із callback-функціями, що виводять у \n"
               "консоль індекс і значення елементів на парних позиціях.\n\n");
            Console_getChar();
      }

      if (switcher == 4) {
        break;
      }
    }

    if (key == 66)
      switcher++;

    if (key == 65)
      switcher--;

    if (switcher == 0)
      switcher = names_count;

    if (switcher == names_count + 1)
      switcher = 1;
  }
}