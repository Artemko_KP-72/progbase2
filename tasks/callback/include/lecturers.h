#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct Lecturer lect;
struct Lecturer {
    char Name[15];
    char LastName[25];
    int lessons_per_week;
};

void lectures_sorting(void);
void print(lect * l);

