#pragma once
#include <stdio.h>
#include <stdlib.h>

void Array_foreachReversed(int arr[], int len, void (*action)(int, int));
void Array_foreach(int arr[], int len, void (*action)(int, int));