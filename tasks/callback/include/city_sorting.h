#pragma once

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <progbase.h>
#include <progbase/console.h>

#define AMOUNT 10
#define WORD_SIZE 25

void simple_types_sorting(void);
void print_cities(char * words);
